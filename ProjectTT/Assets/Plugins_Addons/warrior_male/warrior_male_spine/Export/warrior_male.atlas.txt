
warrior_male.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
arm_l_1
  rotate: false
  xy: 246, 20
  size: 94, 94
  orig: 94, 94
  offset: 0, 0
  index: -1
arm_l_2
  rotate: true
  xy: 357, 86
  size: 84, 94
  orig: 84, 94
  offset: 0, 0
  index: -1
arm_l_3
  rotate: false
  xy: 705, 479
  size: 46, 46
  orig: 46, 46
  offset: 0, 0
  index: -1
arm_r_1
  rotate: false
  xy: 453, 108
  size: 62, 62
  orig: 62, 62
  offset: 0, 0
  index: -1
arm_r_2
  rotate: false
  xy: 705, 527
  size: 46, 72
  orig: 46, 72
  offset: 0, 0
  index: -1
arm_r_3
  rotate: false
  xy: 557, 357
  size: 38, 42
  orig: 38, 42
  offset: 0, 0
  index: -1
body_1
  rotate: false
  xy: 815, 531
  size: 52, 52
  orig: 52, 52
  offset: 0, 0
  index: -1
body_2
  rotate: true
  xy: 942, 906
  size: 108, 66
  orig: 108, 66
  offset: 0, 0
  index: -1
buttock
  rotate: true
  xy: 753, 533
  size: 50, 60
  orig: 50, 60
  offset: 0, 0
  index: -1
head_1
  rotate: true
  xy: 742, 683
  size: 102, 164
  orig: 102, 164
  offset: 0, 0
  index: -1
head_2
  rotate: false
  xy: 601, 435
  size: 102, 164
  orig: 102, 164
  offset: 0, 0
  index: -1
leg_l_1
  rotate: false
  xy: 517, 134
  size: 26, 36
  orig: 26, 36
  offset: 0, 0
  index: -1
leg_l_2
  rotate: true
  xy: 601, 401
  size: 32, 36
  orig: 32, 36
  offset: 0, 0
  index: -1
leg_l_3
  rotate: false
  xy: 246, 2
  size: 36, 16
  orig: 36, 16
  offset: 0, 0
  index: -1
leg_r_1
  rotate: true
  xy: 908, 743
  size: 42, 32
  orig: 42, 32
  offset: 0, 0
  index: -1
leg_r_2
  rotate: false
  xy: 942, 790
  size: 30, 36
  orig: 30, 36
  offset: 0, 0
  index: -1
leg_r_3
  rotate: false
  xy: 453, 90
  size: 36, 16
  orig: 36, 16
  offset: 0, 0
  index: -1
shoulder
  rotate: false
  xy: 942, 828
  size: 66, 76
  orig: 66, 76
  offset: 0, 0
  index: -1
skill_1_a1
  rotate: false
  xy: 2, 78
  size: 242, 36
  orig: 242, 36
  offset: 0, 0
  index: -1
skill_1_a2
  rotate: false
  xy: 2, 40
  size: 242, 36
  orig: 242, 36
  offset: 0, 0
  index: -1
skill_1_a3
  rotate: false
  xy: 2, 2
  size: 242, 36
  orig: 242, 36
  offset: 0, 0
  index: -1
skill_1_b1
  rotate: false
  xy: 2, 844
  size: 368, 170
  orig: 368, 170
  offset: 0, 0
  index: -1
skill_1_b2
  rotate: false
  xy: 2, 672
  size: 368, 170
  orig: 368, 170
  offset: 0, 0
  index: -1
skill_1_b3
  rotate: false
  xy: 372, 844
  size: 368, 170
  orig: 368, 170
  offset: 0, 0
  index: -1
skill_1_b4
  rotate: false
  xy: 2, 500
  size: 368, 170
  orig: 368, 170
  offset: 0, 0
  index: -1
skill_1_b5
  rotate: false
  xy: 372, 672
  size: 368, 170
  orig: 368, 170
  offset: 0, 0
  index: -1
skill_1_c1
  rotate: true
  xy: 2, 116
  size: 382, 69
  orig: 382, 69
  offset: 0, 0
  index: -1
skill_1_c2
  rotate: true
  xy: 73, 116
  size: 382, 69
  orig: 382, 69
  offset: 0, 0
  index: -1
skill_1_c3
  rotate: true
  xy: 144, 116
  size: 382, 69
  orig: 382, 69
  offset: 0, 0
  index: -1
skill_1_c4
  rotate: true
  xy: 215, 116
  size: 382, 69
  orig: 382, 69
  offset: 0, 0
  index: -1
skill_1_c5
  rotate: true
  xy: 286, 116
  size: 382, 69
  orig: 382, 69
  offset: 0, 0
  index: -1
skill_1_c6
  rotate: false
  xy: 372, 601
  size: 382, 69
  orig: 382, 69
  offset: 0, 0
  index: -1
skill_2_1
  rotate: false
  xy: 742, 787
  size: 198, 227
  orig: 198, 227
  offset: 0, 0
  index: -1
skill_2_2
  rotate: true
  xy: 372, 401
  size: 198, 227
  orig: 198, 227
  offset: 0, 0
  index: -1
skill_2_3
  rotate: false
  xy: 357, 172
  size: 198, 227
  orig: 198, 227
  offset: 0, 0
  index: -1
weapon
  rotate: false
  xy: 756, 585
  size: 148, 96
  orig: 148, 96
  offset: 0, 0
  index: -1
