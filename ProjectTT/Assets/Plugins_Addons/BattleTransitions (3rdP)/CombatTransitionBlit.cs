﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CombatTransitionBlit : MonoBehaviour {

    public Material TransitionMaterial;
    public Color ScreenColor;
    [Range(0, 1)]
    public float CutOff;
    public bool Distort;
    [Range (0, 1)]
    public float Fade;

    void OnRenderImage(RenderTexture src, RenderTexture dst)
    {
        if (TransitionMaterial != null)
        {
            TransitionMaterial.SetColor("_Color", ScreenColor);

            TransitionMaterial.SetFloat("_Cutoff", CutOff);

            if (!Distort)
                TransitionMaterial.SetFloat("_Distort", 0);
            else
                TransitionMaterial.SetFloat("_Distort", 1);

            TransitionMaterial.SetFloat("_Fade", Fade);

            Graphics.Blit(src, dst, TransitionMaterial);
        }
    }
}
