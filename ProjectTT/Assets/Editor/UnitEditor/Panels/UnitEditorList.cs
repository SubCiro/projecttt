﻿using UnityEditor;
using UnityEngine;

public partial class UnitEditor {

    Vector2 ListView_scrollPos = new Vector2();
    Vector2 DetailView_scrollPos = new Vector2();
    int ListviewWidth = 200;
    int _ListButtonHeight = 30;

    void ListView()
    {
        ListView_scrollPos = EditorGUILayout.BeginScrollView(ListView_scrollPos, "Box", GUILayout.ExpandHeight(true), GUILayout.Width(ListviewWidth));
        DisplayUnits();

        EditorGUILayout.EndScrollView();
    }

    void DisplayUnits()
    {
        for (int i = 0; i < unitDatabase.Count; i++)
        {
            if (GUILayout.Button(unitDatabase.GetItembyIndex(i).Name, "Box", GUILayout.ExpandWidth(true), GUILayout.Height(_ListButtonHeight)))
                SelectedUnit = new Unit_Info(unitDatabase.GetItembyIndex(i));
        }

        if (GUILayout.Button("Add"))
        {
            SelectedUnit = new Unit_Info();
            unitDatabase.Add(SelectedUnit);
        }
    }
}
