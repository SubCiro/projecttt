﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public partial class UnitEditor
{
    void UnitDetail()
    {        
        GUILayout.BeginHorizontal("Box", GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

        if (SelectedUnit != null)
        {
            DetailView_scrollPos = EditorGUILayout.BeginScrollView(DetailView_scrollPos, "Box", GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            GUILayout.BeginVertical();

            GUILayout.Label("ID: " + SelectedUnit.Id.ToString());

            SelectedUnit.Name = EditorGUILayout.TextField("Name: ", SelectedUnit.Name);
            SelectedUnit.Type = (Unit_Info.UnitType)EditorGUILayout.EnumPopup("Item Type: ", SelectedUnit.Type);

            switch (SelectedUnit.Type)
            {
                case Unit_Info.UnitType.Summoner:
                    SelectedUnit.MaxEnergy = EditorGUILayout.IntField("Energy: ", SelectedUnit.MaxEnergy);
                    SelectedUnit.Energy = SelectedUnit.MaxEnergy;
                    break;
                case Unit_Info.UnitType.Minion:
                    SelectedUnit.EnergyCost = EditorGUILayout.IntField("Energy Cost: ", SelectedUnit.EnergyCost);
                    break;
            }

            SelectedUnit.MaxHealth = EditorGUILayout.IntField("Health: ", SelectedUnit.MaxHealth);
            SelectedUnit.Health = SelectedUnit.MaxHealth;

            SelectedUnit.Move = EditorGUILayout.IntField("Movement: ", SelectedUnit.Move);
            SelectedUnit.Vision = EditorGUILayout.IntField("Vision: ", SelectedUnit.Vision);
            SelectedUnit.Attack = EditorGUILayout.IntField("Attack: ", SelectedUnit.Attack);
            SelectedUnit.Defense = EditorGUILayout.IntField("Defense: ", SelectedUnit.Defense);

            SelectedUnit.FireType = (Unit_Info.UnitFireType)EditorGUILayout.EnumPopup("Fire Type: ", SelectedUnit.FireType);

            switch (SelectedUnit.FireType)
            {
                case Unit_Info.UnitFireType.RANGED:
                    SelectedUnit.FireRange[0] = EditorGUILayout.IntField("Min range: ", SelectedUnit.FireRange[0]);
                    SelectedUnit.FireRange[1] = EditorGUILayout.IntField("Max range: ", SelectedUnit.FireRange[1]);
                    break;
            }

            EditorGUILayout.LabelField("Description: ");
            SelectedUnit.Description = EditorGUILayout.TextArea(SelectedUnit.Description, GUILayout.Height(150));

            // World View Animations
            SelectedUnit.IdleReady = EditorGUILayout.ObjectField("Idle Ready Animation: ", SelectedUnit.IdleReady, typeof(AnimationClip), false) as AnimationClip;
            SelectedUnit.IdleResting = EditorGUILayout.ObjectField("Idle Rest Animation: ", SelectedUnit.IdleResting, typeof(AnimationClip), false) as AnimationClip;
            SelectedUnit.Dying = EditorGUILayout.ObjectField("Death Animation: ", SelectedUnit.Dying, typeof(AnimationClip), false) as AnimationClip;
            SelectedUnit.WalkUp = EditorGUILayout.ObjectField("Walk Up Animation: ", SelectedUnit.WalkUp, typeof(AnimationClip), false) as AnimationClip;
            SelectedUnit.WalkDown = EditorGUILayout.ObjectField("Walk Down Animation: ", SelectedUnit.WalkDown, typeof(AnimationClip), false) as AnimationClip;
            SelectedUnit.WalkSide = EditorGUILayout.ObjectField("Walk Side Animation: ", SelectedUnit.WalkSide, typeof(AnimationClip), false) as AnimationClip;

            // Combat View Animations
            SelectedUnit.Combat_Idle = EditorGUILayout.ObjectField("Combat Idle Animation: ", SelectedUnit.Combat_Idle, typeof(AnimationClip), false) as AnimationClip;
            SelectedUnit.Combat_Dying = EditorGUILayout.ObjectField("Combat Death Animation: ", SelectedUnit.Combat_Dying, typeof(AnimationClip), false) as AnimationClip;
            SelectedUnit.Combat_Attack = EditorGUILayout.ObjectField("Combat Attack Animation: ", SelectedUnit.Combat_Attack, typeof(AnimationClip), false) as AnimationClip;
            SelectedUnit.Combat_Defend = EditorGUILayout.ObjectField("Combat Defend Animation: ", SelectedUnit.Combat_Defend, typeof(AnimationClip), false) as AnimationClip;
            SelectedUnit.Combat_Hit = EditorGUILayout.ObjectField("Combat Hit Animation: ", SelectedUnit.Combat_Hit, typeof(AnimationClip), false) as AnimationClip;

            SelectedUnit.Combat_VFX_Idle = EditorGUILayout.ObjectField("Combat VFX Idle: ", SelectedUnit.Combat_VFX_Idle, typeof(AnimationClip), false) as AnimationClip;
            SelectedUnit.Combat_VFX_Play = EditorGUILayout.ObjectField("Combat VFX Play: ", SelectedUnit.Combat_VFX_Play, typeof(AnimationClip), false) as AnimationClip;

            SelectedUnit.Pvt_IdleReady = EditorGUILayout.Vector2Field("Pvt_IdleReady", SelectedUnit.Pvt_IdleReady);
            SelectedUnit.Pvt_IdleResting = EditorGUILayout.Vector2Field("Pvt_IdleResting", SelectedUnit.Pvt_IdleResting);
            SelectedUnit.Pvt_Dying = EditorGUILayout.Vector2Field("Pvt_Dying", SelectedUnit.Pvt_Dying); 
            SelectedUnit.Pvt_WalkUp = EditorGUILayout.Vector2Field("Pvt_WalkUp", SelectedUnit.Pvt_WalkUp);
            SelectedUnit.Pvt_WalkDown = EditorGUILayout.Vector2Field("Pvt_WalkDown", SelectedUnit.Pvt_WalkDown);
            SelectedUnit.Pvt_WalkSide = EditorGUILayout.Vector2Field("Pvt_WalkSide", SelectedUnit.Pvt_WalkSide);

            SelectedUnit.Pvt_Combat_Idle = EditorGUILayout.Vector2Field("Pvt_Combat_Idle", SelectedUnit.Pvt_Combat_Idle);
            SelectedUnit.Pvt_Combat_Dying = EditorGUILayout.Vector2Field("Pvt_Combat_Dying", SelectedUnit.Pvt_Combat_Dying);
            SelectedUnit.Pvt_Combat_Attack = EditorGUILayout.Vector2Field("Pvt_Combat_Attack", SelectedUnit.Pvt_Combat_Attack);
            SelectedUnit.Pvt_Combat_Defend = EditorGUILayout.Vector2Field("Pvt_Combat_Defend", SelectedUnit.Pvt_Combat_Defend);
            SelectedUnit.Pvt_Combat_Hit = EditorGUILayout.Vector2Field("Pvt_Combat_Hit", SelectedUnit.Pvt_Combat_Hit);
            SelectedUnit.Pvt_Combat_VFX_Idle = EditorGUILayout.Vector2Field("Pvt_Combat_VFX_Idle", SelectedUnit.Pvt_Combat_VFX_Idle);
            SelectedUnit.Pvt_Combat_VFX_Play = EditorGUILayout.Vector2Field("Pvt_Combat_VFX_Play", SelectedUnit.Pvt_Combat_VFX_Play);

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Save"))
            {
                if (unitDatabase.UpdateItem(SelectedUnit.Id, SelectedUnit))
                    Debug.Log("Item Saved: ID: " + SelectedUnit.Id + " Name: " + SelectedUnit.Name);
            }
            if (GUILayout.Button("Delete"))
            {
                if (EditorUtility.DisplayDialog("Delete Unit", "Are you sure you want to delete this unit?", "Ok", "Cancel"))
                {
                    unitDatabase.RemoveById(SelectedUnit.Id);
                    SelectedUnit = null;
                }
            }
            GUILayout.EndHorizontal();

            GUILayout.EndVertical();
            EditorGUILayout.EndScrollView();
        }

        GUILayout.EndHorizontal();
    }
}
