﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class UnitEditor {

    void ButtomStatusBar()
    {
        GUILayout.BeginHorizontal("Box", GUILayout.ExpandWidth(true));
        GUILayout.Label("Units Stored: " + unitDatabase.Count);
        GUILayout.EndHorizontal();
    }
}
