﻿using UnityEditor;
using UnityEngine;

public partial class UnitEditor : EditorWindow
{
    UnitDatabase unitDatabase;
    Unit_Info SelectedUnit;

    const string DATABASE_NAME = @"UnitDatabase.asset";
    const string DATABASE_FOLDER = @"Resources/Databases";

    string Search;

    [MenuItem("#Project_TT/Databases/ Unit database %#u")]
    private static void Init()
    {
        UnitEditor wnd = GetWindow<UnitEditor>();
        wnd.minSize = new Vector2(800, 600);
        wnd.titleContent = new GUIContent("Unit database");
        wnd.Show();
    }

    void OnEnable()
    {
        unitDatabase = CreateInstance<UnitDatabase>();
        unitDatabase = unitDatabase.GetDataBase<UnitDatabase>(DATABASE_FOLDER, DATABASE_NAME);
    }

    private void OnGUI()
    {
        if (unitDatabase == null)
        {
            Debug.LogWarning("Unit database not created");
            return;
        }

        GUILayout.BeginHorizontal();
        ListView();
        UnitDetail();
        GUILayout.EndHorizontal();

        ButtomStatusBar();

    }
}
