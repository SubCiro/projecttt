﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class Option : MonoBehaviour, ISelectHandler, ISubmitHandler// required interface when using the OnSelect method.
{

    public virtual void Select()
    {
        GetComponent<Selectable>().Select();        
    }

    public void OnSelect(BaseEventData eventData)
    {
        UIManager.Instance.PlaySelectOption();
    }

    public void OnSubmit(BaseEventData eventData)
    {
        UIManager.Instance.PlayAcceptOption();
    }
}
