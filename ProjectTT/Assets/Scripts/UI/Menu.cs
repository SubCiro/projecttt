﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Menu : MonoBehaviour
{
    public Option DefaultOption;
    public List<Option> Options;

    public virtual void GetFocus()
    {
        UIManager.eventManager.SetSelectedGameObject(null);
        DefaultOption.Select();
    }

    public virtual void LooseFocus()
    {
        UIManager.eventManager.SetSelectedGameObject(null);
    }
}
