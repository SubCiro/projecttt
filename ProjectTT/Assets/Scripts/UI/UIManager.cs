﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIManager : Manager {

    public new static UIManager Instance;

    GameObject lastselect;
    public static EventSystem eventManager;

    // AUDIO
    public AudioSource UI_GeneralAudioSource;
    public AudioClip Select_Option;
    public AudioClip Accept_Option;

    protected override void Awake () {
        Instance = this;
        eventManager = GetComponentInChildren<EventSystem>();
    }
    
    void Start()
    {
        lastselect = new GameObject();
    }

    void Update()
    {
        if (eventManager.currentSelectedGameObject == null)
        {
            eventManager.SetSelectedGameObject(lastselect);
        }
        else
        {
            lastselect = eventManager.currentSelectedGameObject;
        }
    }

    public void PlaySelectOption()
    {
        if (UI_GeneralAudioSource)
            UI_GeneralAudioSource.PlayOneShot(Select_Option);
    }

    public void PlayAcceptOption()
    {
        if (UI_GeneralAudioSource)
            UI_GeneralAudioSource.PlayOneShot(Accept_Option);
    }
}
