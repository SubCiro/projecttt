﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class PlayerOption : Option
{
    public void EndTurn()
    {
        GameManager.Instance.EndCurrentTurn();
    }

    public void QuitQuickPlay()
    {
        StartCoroutine(SceneChange("MainMenu"));
    }

    IEnumerator SceneChange(string scene)
    {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(scene);
    }
}
