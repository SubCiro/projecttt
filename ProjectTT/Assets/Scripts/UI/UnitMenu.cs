﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitMenu : Menu {

	public void ApplyFilters(UnitOption.UnitOptionFilters filters)
    {
        foreach(Option op in Options)
        {
            if ((filters & op.GetComponent<UnitOption>().OptionFilter) == op.GetComponent<UnitOption>().OptionFilter)
            {
                op.gameObject.SetActive(true);
            }
            else
            {
                op.gameObject.SetActive(false);
            }
        }
    }
}
