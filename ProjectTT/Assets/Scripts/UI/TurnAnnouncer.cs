﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TurnAnnouncer : MonoBehaviour
{
    Animator _anim;
    Text _label;

	// Use this for initialization
	void Awake ()
    {
        _anim = GetComponent<Animator>();
        _label = GetComponentInChildren<Text>();
    }

    public void Play(string message) //fadein
    {
        _anim.SetTrigger("Fade");
        _label.text = message;
    }

    public void Play() //fadeout
    {       
        _anim.SetTrigger("Fade");
    }

    public void EndTurnBegin()
    {
        GameManager.Instance.GetPlayerCurrentTurn().OnStandbyPhase();
    }

    //fade control for spammers
    public void FadingTrue()
    {
        _anim.SetBool("Fading",true);
    }

    public void FadingFalse()
    {
        _anim.SetBool("Fading", false);
    }

    public bool GetFading()
    {
        return _anim.GetBool("Fading");
    }

    public void UpdateFog()
    {
        GameManager.Instance.Map.ResetTransitedCells();

        //fog of war update
        if (GameManager.Instance.Map.FogOfWar)
            GameManager.Instance.Map.UpdateFog();
    }

}
