﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class WinAnnouncer : MonoBehaviour
{
    Animator _anim;
    Text _label;

    // Use this for initialization
    void Awake()
    {
        _anim = GetComponent<Animator>();
        _label = GetComponentInChildren<Text>();
    }

    public void Play(string message) //fadein
    {
        _anim.SetTrigger("Fade");
        _label.text = message;
    }

    //fade control for spammers
    public void FadingTrue()
    {
        _anim.SetBool("Fading", true);
    }

    public void FadingFalse()
    {
        _anim.SetBool("Fading", false);
    }

    public bool GetFading()
    {
        return _anim.GetBool("Fading");
    }
}