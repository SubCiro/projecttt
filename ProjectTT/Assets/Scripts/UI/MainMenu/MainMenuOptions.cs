﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MainMenuOptions : Option {

    public void Quickplay()
    {
        StartCoroutine(SceneChange("QuickPlay"));
    }

    public void Editor()
    {
    }

    public void Store()
    {
    }

    public void QuitGame()
    {
        StartCoroutine(QuitGameCoroutine());
    }

    IEnumerator SceneChange(string scene)
    {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(scene);
    }

    IEnumerator QuitGameCoroutine()
    {
        yield return new WaitForSeconds(1.5f);
        Application.Quit();
    }
}
