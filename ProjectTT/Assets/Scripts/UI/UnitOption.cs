﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UnitOption : Option
{
    public enum UnitOptionFilters
    {
        CAN_REST = 0x0001,
        CAN_ATTACK = 0x0010,
    }

    public UnitOptionFilters OptionFilter;

    public void RestUnit()
    {
        GameManager.Instance.GetPlayerCurrentTurn().RestUnitAfterAction(GameManager.Instance.GetPlayerCurrentTurn().GetSelectedUnit());
    }

    public void AttackUnit()
    {
        GameManager.Instance.GetPlayerCurrentTurn().OnAttackingOrder();
    }
}
