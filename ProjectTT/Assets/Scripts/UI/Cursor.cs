﻿using UnityEngine;
using System.Collections;

public class Cursor : MonoBehaviour
{
    WorldMap_Cell TargetCell;
    WorldMap_Cell aux;

    public void setTargetCell(WorldMap_Cell cell)
    {
        TargetCell = cell;
        transform.position = TargetCell.transform.position; //after i gonna to do animation in cell to cell transition
        aux = TargetCell;
    }
    public WorldMap_Cell getTargetCell()
    {
        return TargetCell;
    }

    #region INPUT_COMMANDS
    public void Up()
    {
        setTargetCell(WorldMap.Instance.getWorldCell(aux.GetPosition() + new Vector2(0, 1)) ? WorldMap.Instance.getWorldCell(aux.GetPosition() + new Vector2(0, 1)) : aux ); 
    }

    public void Down()
    {
        setTargetCell(WorldMap.Instance.getWorldCell(aux.GetPosition() + new Vector2(0, -1)) ? WorldMap.Instance.getWorldCell(aux.GetPosition() + new Vector2(0, -1)) : aux);
    }

    public void Left()
    {
        setTargetCell(WorldMap.Instance.getWorldCell(aux.GetPosition() + new Vector2(-1, 0)) ? WorldMap.Instance.getWorldCell(aux.GetPosition() + new Vector2(-1, 0)) : aux);
    }

    public void Right()
    {
        setTargetCell(WorldMap.Instance.getWorldCell(aux.GetPosition() + new Vector2(1, 0)) ? WorldMap.Instance.getWorldCell(aux.GetPosition() + new Vector2(1, 0)) : aux);
    }
    #endregion
}
