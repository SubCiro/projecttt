﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class User : MonoBehaviour {

    public static User Instance;

    string UserName = "Player";
    int UserLevel = 1;
    int UserGold = 0;
    List<Unit> Inventory = new List<Unit>();
    List<Alignment> Alighments = new List<Alignment>();

    private static bool created = false;

    private void Awake()
    {
        if (!created)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            created = true;
        }
    }

    public string GetUserName()
    {
        return UserName;
    }

    public int GetUserLevel()
    {
        return UserLevel;
    }

    public int GetUserGold()
    {
        return UserGold;
    }

}
