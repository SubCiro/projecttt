﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour {

    public enum PlayerPhases { WaitingForTurn, OnTurnBegin, OnStandbyPhase, OnThinking, OnMovingTroop, OnUnitMenu, OnAttackingOrder, OnAttackAnimation, OnBuying, OnCheckingEnemy, OnPlayerMenu, OnTurnEnd}
    public enum PlayerState { PLAYING, DEAD }

    PlayerPhases CurrentPhase;
    PlayerState CurrentState;

    public Color PlayerColor; 

    //Units
    Unit PlayerUnit;
    List<Unit> Units;
    Unit SelectedUnit;
    Unit CheckingFireUnit;
    bool FogVision = false; //bolean whose allow to a player get visions of his/her units

    bool AttackingOrderFlag = false;

    WorldMap_Cell LastCursorLocation; // last location on player's turn saved


    public void Init()
    {
        SetCurrentState(PlayerState.PLAYING);
        Units = new List<Unit>();
        OnWaitingForTurn();
    }

    public void CreateNewUnit(int ID , Vector2 position) // TENTATIVE
    {
        Unit newUnit = Instantiate(GameManager.Instance.UnitPrefab);
        GameManager.Instance.SetLayerRecursively(newUnit.gameObject, (int)Manager.SceneLayers.Scene_Map);
        newUnit.transform.SetParent(transform);
        newUnit.Init(ID);                                                    
        newUnit.SetUnitState(Unit.UnitState.USED);
        newUnit.SetOwnerPlayer(this);        
        GameManager.Instance.Map.getWorldCell(position).SetUnit(newUnit);
        Units.Add(newUnit);
    }

    public bool CreateNewRandomMinion(Vector2 position) // TENTATIVE
    {
        if (GameManager.Instance.Map.getWorldCell(position).GetUnit())
            return false;

        Unit newUnit = Instantiate(GameManager.Instance.UnitPrefab);
        GameManager.Instance.SetLayerRecursively(newUnit.gameObject, (int)Manager.SceneLayers.Scene_Map);
        newUnit.transform.SetParent(transform);
        newUnit.InitRandom();
        newUnit.SetUnitState(Unit.UnitState.USED);
        newUnit.SetOwnerPlayer(this);
        GameManager.Instance.Map.getWorldCell(position).SetUnit(newUnit);
        Units.Add(newUnit);
        return true;
    }

    public void CreatePlayerUnit(int ID, Vector2 position)
    {
        Unit newUnit = Instantiate(GameManager.Instance.UnitPrefab);
        GameManager.Instance.SetLayerRecursively(newUnit.gameObject, (int)Manager.SceneLayers.Scene_Map);
        newUnit.transform.SetParent(transform);
        newUnit.Init(ID);
        newUnit.SetUnitState(Unit.UnitState.USED);
        newUnit.SetOwnerPlayer(this);
        GameManager.Instance.Map.getWorldCell(position).SetUnit(newUnit);
        PlayerUnit = newUnit;
    } // TENTATIVE

    #region PLAYER_PHASES
    void setPlayerPhase(PlayerPhases phase)
    {
        CurrentPhase = phase;
    }

    public void OnWaitingForTurn()
    {
        setPlayerPhase(PlayerPhases.WaitingForTurn);
    }

    public void OnTurnBegin()
    {
        setPlayerPhase(PlayerPhases.OnTurnBegin);
    }

    public void OnStandbyPhase()
    {
        setPlayerPhase(PlayerPhases.OnStandbyPhase);
        GameManager.Instance.SetCursorEnable(true);
        if (LastCursorLocation != null)
            GameManager.Instance.SetCursorLocation(LastCursorLocation);
        else
        {
            if(PlayerUnit)
                GameManager.Instance.SetCursorLocation(PlayerUnit.GetCurrentPosition());
            else
                GameManager.Instance.SetCursorLocation(Vector2.zero);
        }
        ReadyUnit(PlayerUnit);
        ReadyUnits(Units);
        //
        OnThinking();
    } //this is called in the turn announcer event fadeout

    public void OnThinking()
    {
        setPlayerPhase(PlayerPhases.OnThinking);
    }

    public void OnMovingTroop(Unit unit)
    {
        setPlayerPhase(PlayerPhases.OnMovingTroop);
        SelectUnit(unit);
    }

    public void OnUnitMenu()
    {
        setPlayerPhase(PlayerPhases.OnUnitMenu);
    }

    public void OnAttackingOrder()
    {
        GameManager.Instance.UnitsMenuEnable(false);
        setPlayerPhase(PlayerPhases.OnAttackingOrder);
        SelectedUnit.AttackingOrder();
        AttackingOrderFlag = false;
    }

    public void OnAttackAnimation()
    {
        setPlayerPhase(PlayerPhases.OnAttackAnimation);
    }

    public void OnBuying()
    {
        setPlayerPhase(PlayerPhases.OnBuying);
    }

    public void OnCheckingEnemy()
    {
        setPlayerPhase(PlayerPhases.OnCheckingEnemy);
    }

    public void OnPlayerMenu()
    {
        setPlayerPhase(PlayerPhases.OnPlayerMenu);
    }

    public void OnTurnEnd()
    {
        GameManager.Instance.PlayersMenuEnable(false);
        LastCursorLocation = GameManager.Instance.GetCursorLocation();
        GameManager.Instance.SetCursorEnable(false);
        setPlayerPhase(PlayerPhases.OnTurnEnd); //we must work on effects in this phase
        // . . .
        setPlayerPhase(PlayerPhases.WaitingForTurn);
        RestUnit(PlayerUnit);
        RestUnits(Units);
        GameManager.Instance.NextTurn();
    }
    #endregion

    #region INPUT_COMMANDS
    public void Up()
    {
        switch (CurrentPhase)
        {
            case PlayerPhases.OnThinking:
                GameManager.Instance.cursor.Up();
                break;
            case PlayerPhases.OnMovingTroop:
                GameManager.Instance.cursor.Up();
                SelectedUnit.ChangeDestination();
                break;
            case PlayerPhases.OnAttackingOrder:
                TargetUnitToAttack(Pathfinder.Directions4.NORTH, SelectedUnit);
                break;
        }
    }

    public void Down()
    {
        switch (CurrentPhase)
        {
            case PlayerPhases.OnThinking:
                GameManager.Instance.cursor.Down();
                break;
            case PlayerPhases.OnMovingTroop:
                GameManager.Instance.cursor.Down();
                SelectedUnit.ChangeDestination();
                break;
            case PlayerPhases.OnAttackingOrder:
                TargetUnitToAttack(Pathfinder.Directions4.SOUNTH, SelectedUnit);
                break;
        }
    }

    public void Left()
    {
        switch (CurrentPhase)
        {
            case PlayerPhases.OnThinking:
                GameManager.Instance.cursor.Left();
                break;
            case PlayerPhases.OnMovingTroop:
                GameManager.Instance.cursor.Left();
                SelectedUnit.ChangeDestination();
                break;
            case PlayerPhases.OnAttackingOrder:
                TargetUnitToAttack(Pathfinder.Directions4.WEAST, SelectedUnit);
                break;
        }
    }

    public void Right()
    {
        switch (CurrentPhase)
        {
            case PlayerPhases.OnThinking:
                GameManager.Instance.cursor.Right();
                break;
            case PlayerPhases.OnMovingTroop:
                GameManager.Instance.cursor.Right();
                SelectedUnit.ChangeDestination();
                break;
            case PlayerPhases.OnAttackingOrder:
                TargetUnitToAttack(Pathfinder.Directions4.EAST, SelectedUnit);
                break;
        }
    }

    public void Accept()
    {
        switch(CurrentPhase)
        {
            case PlayerPhases.OnTurnBegin:
                if (!GameManager.Instance.TurnAnnouncerGetFading())
                {
                    GameManager.Instance.TurnAnnouncerPlay();
                }
                break;
            case PlayerPhases.OnThinking:
                Unit unit = GameManager.Instance.GetCursorLocation().GetUnit();
                if (unit)// is a unit in this cell?
                {
                    if(Units.Contains(unit) || unit == PlayerUnit) // it is one of my units?
                    {                        
                        switch (unit.GetState()) // which state is my unit
                        {
                            case Unit.UnitState.READY:
                                OnMovingTroop(unit);
                                break;
                        }
                    }
                }
                break;
            case PlayerPhases.OnMovingTroop:
                if(GameManager.Instance.Pathfinder.IfThisCellIsInNav(GameManager.Instance.cursor.getTargetCell()))
                {
                    if (!GameManager.Instance.cursor.getTargetCell().GetUnit())
                    {
                        MoveUnit(SelectedUnit);
                    }
                    else if(GameManager.Instance.cursor.getTargetCell().GetUnit() == SelectedUnit)
                    {
                        MoveUnit(SelectedUnit);
                    }
                    else if(GameManager.Instance.cursor.getTargetCell().GetUnit() && !GameManager.Instance.cursor.getTargetCell().IsHighLighted())
                    {
                        MoveUnit(SelectedUnit);
                    }

                    //OnThinking();
                }
                break;
            case PlayerPhases.OnAttackingOrder:
                if (!AttackingOrderFlag)
                    AttackingOrderFlag = true;
                else
                {
                    OnAttackAnimation();
                    GameManager.Instance.SceneMap_Cam.BattleTransitionFade();
                    GameManager.Instance.CombatScene.SetBattle(SelectedUnit, GameManager.Instance.GetCursorLocation().GetUnit());

                    //quit this comment to make a quick mode
                    //SelectedUnit.AttackUnit(GameManager.Instance.GetCursorLocation().GetUnit());
                    //GameManager.Instance.Pathfinder.ResetFireCells();
                    //RestUnit(SelectedUnit);
                }
                break;
        }
    }

    public void Cancel()
    {
        switch(CurrentPhase)
        {
            case PlayerPhases.OnThinking: //we need to improve the manage of menus 
                CheckingFireUnit = GameManager.Instance.GetCursorLocation().GetUnit();
                if (CheckingFireUnit)
                {
                    if (GameManager.Instance.GetCursorLocation().IsHighLighted())
                    {
                        CheckingFireUnit.FireRangeCheckOn();
                    }
                    else
                    {
                        GameManager.Instance.PlayersMenuEnable(true);
                        OnPlayerMenu();
                    }
                }
                else
                {
                    GameManager.Instance.PlayersMenuEnable(true);
                    OnPlayerMenu();
                }

                break;
            case PlayerPhases.OnPlayerMenu: //we need to improve the manage of menus
                GameManager.Instance.PlayersMenuEnable(false);                
                OnThinking();
                break;
            case PlayerPhases.OnMovingTroop:
                ReadytUnit(SelectedUnit);
                OnThinking();
                break;
            case PlayerPhases.OnUnitMenu:
                if(SelectedUnit.GetState() != Unit.UnitState.ONTRASIT)
                    CancelUnitCommand(SelectedUnit);
                break;
            case PlayerPhases.OnAttackingOrder:
                CancelAttackOrder(SelectedUnit);
                break;
        }
    }

    public void AcceptUp()
    {

    }

    public void CancelUp()
    {
        switch(CurrentPhase)
        {
            case PlayerPhases.OnThinking:
                if (CheckingFireUnit)
                    CheckingFireUnit.FireRangeCheckOff();
                break;
        }
    }
    #endregion

    #region UNIT_COMMAND
    public void SelectUnit(Unit unit)
    {
        SelectedUnit = unit;
        unit.Select();
    }

    public void ReadytUnit(Unit unit)
    {
        unit.Ready();        
        GameManager.Instance.Pathfinder.ResetNavMesh();
        GameManager.Instance.Pathfinder.ResetPathMesh();
        unit = null;
    }

    public void RestUnitAfterAction(Unit unit)
    {
        unit.RestAfterAction();
        unit = null;
        GameManager.Instance.UnitsMenuEnable(false);
        OnThinking();
    }

    public void ReadyUnit(Unit unit)
    {
        unit.Ready();
    }

    public void RestUnit(Unit unit)
    {
        unit.SetUnitState(Unit.UnitState.USED);
    }

    public void ReadyUnits(List<Unit> Units)
    {
        foreach(Unit u in Units)
        {
            u.Ready();
        }
    }

    public void RestUnits(List<Unit> Units)
    {
        foreach (Unit u in Units)
        {
            u.SetUnitState(Unit.UnitState.USED);
        }
    }

    public void MoveUnit(Unit unit)
    {
        unit.Move();
    }

    public void CancelUnitCommand(Unit unit)
    {
        unit.SetCurrentPosition(unit.GetCurrentPosition());
        unit.LoadTansitableCells();
        unit.LoadPathToMove(GameManager.Instance.cursor.getTargetCell());
        GameManager.Instance.UnitsMenuEnable(false);
        OnMovingTroop(SelectedUnit);
    }

    public void CancelAttackOrder(Unit unit)
    {
        GameManager.Instance.Pathfinder.ResetFireCells();
        GameManager.Instance.UnitsMenuEnable(true);

        //filtering options of unit
        if (unit.GetAtacableCellsCount() > 0)
        {
            GameManager.Instance.UnitsMenuApplyFilters(UnitOption.UnitOptionFilters.CAN_REST | UnitOption.UnitOptionFilters.CAN_ATTACK);
        }
        else if (unit.GetAtacableCellsCount() == 0)
        {
            GameManager.Instance.UnitsMenuApplyFilters(UnitOption.UnitOptionFilters.CAN_REST);
        }

        GameManager.Instance.SetCursorLocation(unit.GetDestination());
        OnUnitMenu();
        unit.SetUnitState(Unit.UnitState.SELECTED);
    }

    public void TargetUnitToAttack(Pathfinder.Directions4 direction, Unit unit)
    {
        unit.TargetUnitToAttack(direction);
    }
    #endregion

    #region INFO
    public Unit GetSelectedUnit()
    {
        return SelectedUnit;
    }

    public void UnitDestroyed(Unit unit)
    {
        if (unit != PlayerUnit)
        {
            Units.Remove(unit);
            if(Units.Count == 0 )
            {
                GameManager.Instance.LoosePlayer(this);
            }
        }
        else
        {
            GameManager.Instance.LoosePlayer(this);
        }
        Destroy(unit.gameObject);
    } // Notify when a unit of this player is goint to be destroyed

    public void SetFogVision(bool vision)
    {
        FogVision = vision;
    }

    public bool GetFogVision()
    {
        return FogVision;
    }

    public void SetCurrentState(PlayerState state)
    {
        CurrentState = state;
    }

    public PlayerState GetCurrentState()
    {
        return CurrentState;
    }
    #endregion
}
