﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GameManager : Manager
{    
    public enum GamePhases { INITIALIZING, INGAME, ENDGAME }

    //SelfInstance
    public static GameManager Instance;

    //Phase
    GamePhases CurrentPhase;

    //Cameras
    public CameraAnimations SceneMap_Cam;
    public CameraMovement SceneMap_Cam_Movement;
    public CameraMovement Combat_Cam_Movement;

    //Scenes
    public CombatScene CombatScene;

    //Players
    public int numberOfPlayers;
    public Player PlayerPrefab;
    List<Player> Players;
    int playerturn = -1;
    int gameturn;

    //Units
    public Unit UnitPrefab;
    public int MinionQuantity = 5;

    //UI
    public Cursor cursor; //NOTE we must to make it private
    public TurnAnnouncer turnAnnouncer; //NOTE we must to make it private
    public WinAnnouncer winAnnouncer; //NOTE we must to make it private
    public Menu PlayersMenu; //TENTATIVE we must work to optimize this //Menu of player options
    public UnitMenu UnitsMenu; //TENTATIVE we must work to optimize this //Menu of unit options

    //WorldMap
    //public int WorldMapFile; // in the future the map will be loaded by file, for now we need to instanciate by ourselves.
    public WorldMap Map;
    public Pathfinder Pathfinder; // it provides path findings services
    public Vector2 WorldSize; //Tentative... waiting for a file

    //Databases
    public UnitDatabase unitDatabase;

    #region INITIALIZE_GAME
    protected override void Awake()
    {
        base.Awake();
        Instance = this;
        Players = new List<Player>();

        SetGamePhase(GamePhases.INITIALIZING);
        CreateWorldMap();
        CreateCursor();
        CreatePlayers();
        SetPlayerTurns(true);
        SetGamePhase(GamePhases.INGAME);
        NextTurn();
    }

    void CreateWorldMap()
    {
        Map = Instantiate(Map);
        Map.gameObject.layer = (int)SceneLayers.Scene_Map;
        Map.transform.SetParent(GameObject.Find("_Scene").transform);
        Map.name = Map.name.Split('(')[0];
        Map.SetWorldSize(new Vector2(WorldSize.y, WorldSize.x));
        Map.gameObject.SetActive(true);
    }

    void CreateCursor()
    {
        cursor = Instantiate(cursor);
        cursor.gameObject.layer = (int)SceneLayers.Scene_Map;
        cursor.transform.SetParent(GameObject.Find("_GameUI").transform);
        cursor.name = cursor.name.Split('(')[0];
        cursor.GetComponent<SpriteRenderer>().sortingLayerName = DrawGameLayers.MapCursor.ToString();
    }

    void CreatePlayers()
    {
        for (int i = 0; i < numberOfPlayers; i++)
        {
            Player newplayer = Instantiate(PlayerPrefab);
            newplayer.gameObject.layer = (int)SceneLayers.Scene_Map;
            newplayer.transform.SetParent(GameObject.Find("_Players").transform);
            newplayer.name = newplayer.name.Split('(')[0] + " " + i;
            newplayer.Init();


            //load formation info
            //hardcoded for 2 players
            if (i == 0)
            {
                newplayer.PlayerColor = Color.red;
                newplayer.CreatePlayerUnit(11, new Vector2(4, 0));                
                for (int j = 0; j < MinionQuantity; j++)
                {
                    while (!newplayer.CreateNewRandomMinion(new Vector2(Random.Range(0, WorldSize.x), Random.Range(0, 4))))
                    {
                        //...
                    }
                }
            }
            else if (i == 1)
            {
                newplayer.PlayerColor = Color.blue;
                newplayer.CreatePlayerUnit(11, new Vector2(4, WorldSize.y - 1));                
                for (int j = 0; j < MinionQuantity; j++)
                {
                    while (!newplayer.CreateNewRandomMinion(new Vector2(Random.Range(0, WorldSize.x), Random.Range(WorldSize.y - 4, WorldSize.y))))
                    {
                        //...
                    }
                }
            }

            Players.Add(newplayer);
        }
    }

    void SetPlayerTurns(bool random)
    {
        playerturn = 0;
        gameturn = 1;

        if (random)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                Player pTemp = Players[i];
                int k = Random.Range(i, Players.Count);
                Players[i] = Players[k];
                Players[k] = pTemp;
            }
        }
    }
    #endregion

    #region TURNS_LOGIC
    public Player GetPlayerCurrentTurn()
    {
        if (playerturn != -1)
            return Players[playerturn];
        else
            return null;
    }

    public void EndCurrentTurn()
    {
        Players[playerturn].SetFogVision(false);
        Players[playerturn].OnTurnEnd();
    }

    public void NextTurn()
    {
        playerturn++;
        if (playerturn >= Players.Count)
        {
            playerturn = 0;
            gameturn++;
        }

        if (Players[playerturn].GetCurrentState() == Player.PlayerState.DEAD)
        {
            NextTurn();
        }
        else
        {
            TurnAnnouncerPlay(Players[playerturn].name + "'s Turn");
            Players[playerturn].OnTurnBegin();
            Players[playerturn].SetFogVision(true);
        }
    }

    public void TurnAnnouncerPlay(string message)
    {
        turnAnnouncer.Play(message);
    }

    public void TurnAnnouncerPlay()
    {
        turnAnnouncer.Play();
    }

    public bool TurnAnnouncerGetFading()
    {
        return turnAnnouncer.GetFading();
    }
    #endregion    

    #region COMBAT ANIMATION
    Unit Attacker;
    Unit Defender;

    public void AttackAnimationBegin()
    {
        CombatScene.gameObject.SetActive(true);
    }

    public void CombatSceneEnd(Unit A, Unit D)
    {
        CombatScene.gameObject.SetActive(false);
        SceneMap_Cam.BattleTransitionFade();
        Pathfinder.ResetFireCells();
        Attacker = A;
        Defender = D;
    }

    public void AttackAnimationEnd()
    {
        if (Attacker.GetHealth() <= 0 && Defender.GetHealth() <= 0)
        {
            //Debug.Log("both dies");
            Attacker.SetUnitState(Unit.UnitState.ONDYING);
            Defender.SetUnitState(Unit.UnitState.ONDYING);
            StartCoroutine(ContinueTurnAfetCombat(.6f));
        }
        else if (Attacker.GetHealth() > 0 && Defender.GetHealth() <= 0)
        {
            //Debug.Log("ataker wins");
            Defender.SetUnitState(Unit.UnitState.ONDYING);
            StartCoroutine(RestUnitAfterCombat(.6f));
        }
        else if (Attacker.GetHealth() <= 0 && Defender.GetHealth() > 0)
        {
            //Debug.Log("Defender wins");
            Attacker.SetUnitState(Unit.UnitState.ONDYING);
            StartCoroutine(ContinueTurnAfetCombat(.6f));
        }
        else if (Attacker.GetHealth() > 0 && Defender.GetHealth() > 0)
        {
            //Debug.Log("no deaths");
            StartCoroutine(RestUnitAfterCombat(0));
        }
    }

    IEnumerator ContinueTurnAfetCombat(float duration)
    {
        yield return new WaitForSeconds(duration);
        GetPlayerCurrentTurn().OnThinking();
    }

    IEnumerator RestUnitAfterCombat(float duration)
    {
        yield return new WaitForSeconds(duration);
        GetPlayerCurrentTurn().RestUnitAfterAction(GetPlayerCurrentTurn().GetSelectedUnit());
    }
    #endregion

    #region INPUT_COMMANDS
    public override void Up()
    {
        switch (GetGamePhase())
        {
            case GamePhases.INGAME:
                Players[playerturn].Up();
                break;
            case GamePhases.ENDGAME:
                if (!WinAnnouncerGetFading())
                {
                    SceneManager.LoadScene(0);
                }
                break;
        }
    }

    public override void Down()
    {
        switch (GetGamePhase())
        {
            case GamePhases.INGAME:
                Players[playerturn].Down();
                break;
            case GamePhases.ENDGAME:
                if (!WinAnnouncerGetFading())
                {
                    SceneManager.LoadScene(0);
                }
                break;
        }
    }

    public override void Left()
    {
        switch (GetGamePhase())
        {
            case GamePhases.INGAME:
                Players[playerturn].Left();
                break;
            case GamePhases.ENDGAME:
                if (!WinAnnouncerGetFading())
                {
                    SceneManager.LoadScene(0);
                }
                break;
        }
    }

    public override void Right()
    {
        switch (GetGamePhase())
        {
            case GamePhases.INGAME:
                Players[playerturn].Right();
                break;
            case GamePhases.ENDGAME:
                if (!WinAnnouncerGetFading())
                {
                    SceneManager.LoadScene(0);
                }
                break;
        }

    }

    public override void Accept()
    {
        switch (GetGamePhase())
        {
            case GamePhases.INGAME:
                Players[playerturn].Accept();
                break;
            case GamePhases.ENDGAME:
                if (!WinAnnouncerGetFading())
                {
                    SceneManager.LoadScene(0);
                }
                break;
        }
    }

    public override void Cancel()
    {
        switch (GetGamePhase())
        {
            case GamePhases.INGAME:
                Players[playerturn].Cancel();
                break;
            case GamePhases.ENDGAME:
                if (!WinAnnouncerGetFading())
                {
                    SceneManager.LoadScene(0);
                }
                break;
        }
    }

    public override void AcceptUp()
    {
        switch (GetGamePhase())
        {
            case GamePhases.INGAME:
                Players[playerturn].AcceptUp();
                break;
        }
    }

    public override void CancelUp()
    {
        switch (GetGamePhase())
        {
            case GamePhases.INGAME:
                Players[playerturn].CancelUp();
                break;
        }
    }
    #endregion

    #region UTILITIES
    public void SetLayerRecursively(GameObject obj, int newLayer)
    {
        obj.layer = newLayer;

        foreach (Transform child in obj.transform)
        {
            SetLayerRecursively(child.gameObject, newLayer);
        }
    }
    #endregion    

    #region GAME_INFORMATION
    public void LoosePlayer(Player ply) // WIN CONDITION
    {
        ply.SetCurrentState(Player.PlayerState.DEAD);
        // do loose animation for the player

        // check how many player still playing
        if (CountLivePlayers() == 1)
        {
            SetGamePhase(GamePhases.ENDGAME);
            winAnnouncer.Play(PlayersAlive()[0].name + " Wins !!");
        }
    }

    public int CountLivePlayers()
    {
        int livePly = 0;
        for (int i = 0; i < Players.Count; i++)
        {
            if (Players[i].GetCurrentState() == Player.PlayerState.PLAYING)
            {
                livePly++;
            }
        }
        return livePly;
    }

    public List<Player> PlayersAlive()
    {
        List<Player> plyrsAlive = new List<Player>();
        for (int i = 0; i < Players.Count; i++)
        {
            if (Players[i].GetCurrentState() == Player.PlayerState.PLAYING)
            {
                plyrsAlive.Add(Players[i]);
            }
        }
        return plyrsAlive;
    }

    public void SetGamePhase(GamePhases phase)
    {
        CurrentPhase = phase;
    }

    public GamePhases GetGamePhase()
    {
        return CurrentPhase;
    }

    public void SetCursorLocation(Vector2 position)
    {
        cursor.setTargetCell(Map.getWorldCell(position));
    }

    public void SetCursorLocation(WorldMap_Cell cell)
    {
        cursor.setTargetCell(cell);
    }

    public WorldMap_Cell GetCursorLocation()
    {
        return cursor.getTargetCell();
    }

    public void SetCursorEnable(bool enable)
    {
        cursor.gameObject.SetActive(enable);
    }
    #endregion

    public void ShakeCameras(float Duration, float Intensity)
    {
        SceneMap_Cam_Movement.shakeTimer = Duration;
        SceneMap_Cam_Movement.shakeAmount = Intensity;

        Combat_Cam_Movement.shakeTimer = Duration;
        Combat_Cam_Movement.shakeAmount = Intensity;
    }

    public void WinAnnouncerPlay(string message)
    {
        winAnnouncer.Play(message);
    }

    public bool WinAnnouncerGetFading()
    {
        return winAnnouncer.GetFading();
    }

    public void PlayersMenuEnable(bool enable)
    {
        PlayersMenu.gameObject.SetActive(enable);

        if (enable)
            PlayersMenu.GetFocus();
        else
            PlayersMenu.LooseFocus();
    } //we need to improve the manage of menus

    public void UnitsMenuEnable(bool enable)
    {
        UnitsMenu.gameObject.SetActive(enable);

        if (enable)
            UnitsMenu.GetFocus();
        else
            UnitsMenu.LooseFocus();
    } //we need to improve the manage of menus

    public void UnitsMenuApplyFilters(UnitOption.UnitOptionFilters filters)
    {
        UnitsMenu.ApplyFilters(filters);
    }

}