﻿/**
 * Note: check the Unity's eventsystem to change the AcceptButton and CancelButton
 * 
 * **/

using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

//all the players controls by one input for now //TENTATIVE
public class DemoInput : MonoBehaviour
{
    public string VerticalAxis = "Vertical";
    public string HorizontalAxis = "Horizontal";
    public string AcceptButton = "Accept_Command";
    public string CancelButton = "Cancel_Command";
    public string ReloadButton = "Reload_Command"; //DEV
    public string FogToggleButton = "FogToggle_Command"; //DEV
    public float HoldingDelay = .2f;
    float maxHoldingDelay;

    float lastVerticalAxis = 0;
    float lastHorizontalAxis = 0;

    void Awake()
    {
        maxHoldingDelay = HoldingDelay;
    }

    void Update()
    {
        #region VERTICAL_AXIS
        if (Input.GetAxisRaw(VerticalAxis) == 1)
        {
            if (Input.GetAxisRaw(VerticalAxis) == lastVerticalAxis)
            {
                HoldingDelay -= Time.fixedDeltaTime;
                if (HoldingDelay <= 0)
                {
                    Manager.Instance.Up();
                    HoldingDelay = maxHoldingDelay;
                }
            }
            else
            {
                Manager.Instance.Up();
                HoldingDelay = maxHoldingDelay;
            }
            lastVerticalAxis = 1;
        }
        else if (Input.GetAxisRaw(VerticalAxis) == -1)
        {
            if (Input.GetAxisRaw(VerticalAxis) == lastVerticalAxis)
            {
                HoldingDelay -= Time.fixedDeltaTime;
                if (HoldingDelay <= 0)
                {
                    Manager.Instance.Down();
                    HoldingDelay = maxHoldingDelay;
                }
            }
            else
            {
                Manager.Instance.Down();
                HoldingDelay = maxHoldingDelay;
            }
            lastVerticalAxis = -1;
        }
        else if(Input.GetAxisRaw(VerticalAxis) == 0)
        {
            lastVerticalAxis = 0;
        }
        #endregion

        #region HORIZONTAL_AXIS
        if (Input.GetAxisRaw(HorizontalAxis) == 1)
        {
            if (Input.GetAxisRaw(HorizontalAxis) == lastHorizontalAxis)
            {
                HoldingDelay -= Time.fixedDeltaTime;
                if (HoldingDelay <= 0)
                {
                    Manager.Instance.Right();
                    HoldingDelay = maxHoldingDelay;
                }
            }
            else
            {
                Manager.Instance.Right();
                HoldingDelay = maxHoldingDelay;
            }
            lastHorizontalAxis = 1;
        }
        else if (Input.GetAxisRaw(HorizontalAxis) == -1)
        {
            if (Input.GetAxisRaw(HorizontalAxis) == lastHorizontalAxis)
            {
                HoldingDelay -= Time.fixedDeltaTime;
                if (HoldingDelay <= 0)
                {
                    Manager.Instance.Left();
                    HoldingDelay = maxHoldingDelay;
                }
            }
            else
            {
                Manager.Instance.Left();
                HoldingDelay = maxHoldingDelay;
            }
            lastHorizontalAxis = -1;
        }
        else if (Input.GetAxisRaw(HorizontalAxis) == 0)
        {
            lastHorizontalAxis = 0;
        }
        #endregion

        #region ACCEPT_CANCEL
        if (Input.GetButtonDown(AcceptButton))
        {
            Manager.Instance.Accept();
        }
        else if (Input.GetButtonDown(CancelButton))
        {
            Manager.Instance.Cancel();
        }

        if (Input.GetButtonUp(CancelButton))
        {
            Manager.Instance.CancelUp();
        }

        if (Input.GetButtonUp(AcceptButton))
        {
            Manager.Instance.AcceptUp();
        }
        #endregion

        #region DEV_COMMANDS

        if(Input.GetButtonDown(ReloadButton))
        {
            SceneManager.LoadScene("QuickPlay");
        }

        if(Input.GetButtonDown(FogToggleButton))
        {
            if(GameManager.Instance)
                GameManager.Instance.Map.FogOfWar = !GameManager.Instance.Map.FogOfWar;
        }

        #endregion
    }
}
