﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAnimations : MonoBehaviour {

    Animator _anim;

	// Use this for initialization
	void Start () {
        _anim = GetComponent<Animator>();
	}

    public void BattleTransitionFade()
    {
        _anim.SetTrigger("Fade");
    }
    
    public void BattleAnimationBegin()
    {
        GameManager.Instance.AttackAnimationBegin();
    }

    public void BattleAnimationEnd()
    {
        GameManager.Instance.AttackAnimationEnd();
    }
}
