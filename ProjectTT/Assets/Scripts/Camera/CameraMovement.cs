﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    public float shakeTimer, shakeAmount;
    Vector3 originalPos;
    Vector3 startPosition;

    public Vector2 Margin;
    public Vector2 Damp;
    Vector3 min;
    Vector3 max;
    public bool follow;
    public bool follow_x;
    public bool follow_y;

    void Start()
    {
        startPosition = transform.position;
    }

    public void SetCameraBounds(Bounds bounds)
    {
        min = bounds.min;
        max = bounds.max;
    }

    // Update is called once per frame
    void Update ()
    {
        #region MOVEMENT_CURSOR

        float x = transform.position.x;
        float y = transform.position.y;

        if ((GetComponent<Camera>().orthographicSize * ((float)Screen.width / Screen.height)) > (GameManager.Instance.WorldSize.x / 2))
            follow_x = false;
        else
            follow_x = true;

        if (GetComponent<Camera>().orthographicSize > (GameManager.Instance.WorldSize.y / 2))
            follow_y = false;
        else
            follow_y = true;

        if (follow_x)
        {
            if (Mathf.Abs(x - GameManager.Instance.cursor.transform.position.x) > Margin.x)
                x = Mathf.Lerp(x, GameManager.Instance.cursor.transform.position.x, Damp.x * Time.deltaTime);

            float cameraHalfWidth = GetComponent<Camera>().orthographicSize * ((float)Screen.width / Screen.height);
            x = Mathf.Clamp(x, min.x + cameraHalfWidth, max.x - cameraHalfWidth);
        }
        else
        {
            x = 0;
        }

        if (follow_y)
        {
            if (Mathf.Abs(y - GameManager.Instance.cursor.transform.position.y) > Margin.y)
                y = Mathf.Lerp(y, GameManager.Instance.cursor.transform.position.y, Damp.y * Time.deltaTime);

            y = Mathf.Clamp(y, min.y + GetComponent<Camera>().orthographicSize, max.y - GetComponent<Camera>().orthographicSize);
        }
        else
        {
            y = 0;
        }

        if (follow)
            transform.position = new Vector3(x, y, transform.position.z);

        #endregion

        #region SHAKE_MECHANIC
        if (follow)
            originalPos = transform.position;
        else
            originalPos = startPosition;

        if (shakeTimer > 0)
        {
            Vector2 shakePos = Random.insideUnitCircle * shakeAmount;

            transform.position = new Vector3(transform.position.x + shakePos.x, transform.position.y + shakePos.y, transform.position.z);

            shakeTimer -= Time.deltaTime;
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, originalPos, Time.fixedDeltaTime * 20);
        }
#endregion
    }
}
