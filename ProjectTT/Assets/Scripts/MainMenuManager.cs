﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : Manager {

    public Menu mainMenu;

	// Use this for initialization
	protected override void Awake ()
    {
        // check the player if is created
        // load data from the player
        // else 
        // create new player profile

        // show game title
        base.Awake();
        mainMenu.GetFocus();
	}

    public override void Accept()
    {
        base.Accept();

    }
}
