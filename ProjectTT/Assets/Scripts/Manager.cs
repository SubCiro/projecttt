﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour {

    public static Manager Instance;
    public enum DrawGameLayers { NatureSoil, NatureStructure, Building, Nav, Pathfinder, Unit, MapCursor }
    public enum SceneLayers { Scene_Map = 8, Scene_Combat = 9 }    

    protected virtual void Awake()
    {
        Instance = this;
    }

    #region INPUT_COMMANDS
    public virtual void Up()
    {
    }

    public virtual void Down()
    {
    }

    public virtual void Left()
    {
    }

    public virtual void Right()
    {

    }

    public virtual void Accept()
    {
    }

    public virtual void Cancel()
    {
    }

    public virtual void AcceptUp()
    {
    }

    public virtual void CancelUp()
    {
    }
    #endregion
}
