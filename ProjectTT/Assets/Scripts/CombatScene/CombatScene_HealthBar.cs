﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombatScene_HealthBar : MonoBehaviour {

    public Image HealthImage;
    float value = 1;
    float damp = 4;

	public void SetValue(float val, bool flatanim)
    {
        value = val;
        if (flatanim)
            HealthImage.fillAmount = value;
    }

    public float GetValue()
    {
        return value;
    }
	
	// Update is called once per frame
	void Update () {
        HealthImage.fillAmount = Mathf.Lerp(HealthImage.fillAmount, value, Time.deltaTime * damp);
	}
}
