﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatScene_UnitAnimationEvents : MonoBehaviour {

    public CombatScene parent;
    public CombatScene_Unit parentUnit;

    public enum Combat_Animation
    {
        IDLE,
        DYING,
        ATTACK,
        DEFEND,
        HIT
    }

    public void DeathEnd()
    {
        parent.DeathEnd();
    }

    public void AttackEnd()
    {
        parent.AttackEnd();
    }

    public void CounterAttackEnd()
    {
        parent.CounterAttackEnd();
    }

    public void GiveAttackDamage()
    {
        parent.GiveAttackDamage();
    }

    public void GiveCounterAttackDamage()
    {
        parent.GiveCounterAttackDamage();
    }

    public void SpawnAttackVFX()
    {
        parent.SpawnAttackVFX();
    }

    /*public void SpawnCounterAttackVFX()
    {
        parent.SpawnCounterAttackVFX();
    }*/

    public void UpdateOffsetPivot(string state)
    {
        Combat_Animation fadeState = (Combat_Animation)Combat_Animation.Parse(typeof(Combat_Animation), state);
        parentUnit.SetOffetPivot(fadeState);
    }

}
