﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatScene_Unit_VFX : MonoBehaviour
{
    public CombatScene parent;

    public void GiveAttackDamage()
    {
        parent.GiveAttackDamage();
    }
}
