﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatScene : MonoBehaviour {

    public CombatScene_Unit attakerUnit;
    public CombatScene_Unit defenderUnit;

    public void SetBattle(Unit Ataker, Unit Defender)
    {
        attakerUnit.SetUnit(Ataker);
        defenderUnit.SetUnit(Defender);

        attakerUnit.setRangePositions(Ataker.GetFireType());
        defenderUnit.setRangePositions(Ataker.GetFireType());
    }

    private void OnEnable()
    {
        attakerUnit.SetLive(true);
        defenderUnit.SetLive(true);
        attakerUnit.DoAttackAnimation();
    }

    public void AttackEnd()
    {
        if (defenderUnit.GetUnit().GetHealth() <= 0)
        {
            defenderUnit.SetLive(false);
        }
        else if (attakerUnit.GetUnit().GetHealth() <= 0)
        {
            attakerUnit.SetLive(false);
        }
        else if (attakerUnit.GetUnit().GetFireType() == Unit_Info.UnitFireType.MEELE && defenderUnit.GetUnit().GetHealth() > 0 && defenderUnit.GetUnit().GetUnitType() != Unit_Info.UnitType.Summoner && defenderUnit.GetUnit().GetFireType() != Unit_Info.UnitFireType.RANGED)
        {
            defenderUnit.DoCounterAttackAnimation();
        }
        else
        {
            StartCoroutine(CombatAnimationEnd());
        }
    }

    public void CounterAttackEnd()
    {
        StartCoroutine(CombatAnimationEnd());
    }

    public void DeathEnd()
    {
        StartCoroutine(CombatAnimationEnd());
    }

    public void SpawnAttackVFX()
    {
        attakerUnit.PlayVFX();
    }

    /*public void SpawnCounterAttackVFX()
    {
        defenderUnit.PlayVFX();
    }*/

    IEnumerator CombatAnimationEnd()
    {
        yield return new WaitForSeconds(.3f);
        GameManager.Instance.CombatSceneEnd(attakerUnit.GetUnit(), defenderUnit.GetUnit());
    }

    public void GiveAttackDamage()
    {
        attakerUnit.GetUnit().AttackUnit(defenderUnit.GetUnit());
        defenderUnit.UpdateValues(false);
        defenderUnit.Hit();
    }

    public void GiveCounterAttackDamage()
    {
        defenderUnit.GetUnit().CounterAttackUnit(attakerUnit.GetUnit());
        attakerUnit.UpdateValues(false);
        attakerUnit.Hit();
    }
}
