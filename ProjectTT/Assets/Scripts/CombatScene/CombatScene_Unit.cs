﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatScene_Unit : MonoBehaviour {

   public enum Combat_Rol
    {
        ATTACKING,
        DEFENDING
    }

    Unit unit;
    public Animator _anim;
    public Animator _animVFX;
    AnimatorOverrideController _animOverride;
    AnimatorOverrideController _animVFXOverride;
    protected AnimationClipOverrides clipOverrides;
    protected AnimationClipOverrides clipVFXOverrides;

    public CombatScene_HealthBar healthbar;
    public bool Initialized;
    public Combat_Rol rol;

    public Transform MeelePos;
    public Transform RangePos;
    bool Initialize;
    Transform UnitSpriteOffset;

    private void Init()
    {
        _animOverride = new AnimatorOverrideController(_anim.runtimeAnimatorController);
        _anim.runtimeAnimatorController = _animOverride;
        clipOverrides = new AnimationClipOverrides(_animOverride.overridesCount);
        _animOverride.GetOverrides(clipOverrides);

        _animVFXOverride = new AnimatorOverrideController(_animVFX.runtimeAnimatorController);
        _animVFX.runtimeAnimatorController = _animVFXOverride;
        clipVFXOverrides = new AnimationClipOverrides(_animVFXOverride.overridesCount);
        _animVFXOverride.GetOverrides(clipVFXOverrides);

        UnitSpriteOffset = _anim.transform.parent;

        Initialize = true;
    }

    public void SetUnit(Unit u)
    {
        unit = u;
        if (!Initialize)
            Init();
        unit.UpdateCombatAnimationsClips(this);
        UpdateValues(true);
    }

    public void UpdateCombatAnimationsClips(Unit_Info info)
    {
        clipOverrides["Unit_Combat_idle"] = info.Combat_Idle;
        clipOverrides["Unit_Combat_Die"] = info.Combat_Dying;

        if(rol == Combat_Rol.ATTACKING)
            clipOverrides["Unit_Combat_Atk"] = info.Combat_Attack;
        else if(rol == Combat_Rol.DEFENDING)
            clipOverrides["Unit_Combat_Def"] = info.Combat_Defend;

        clipOverrides["Unit_Combat_Hit"] = info.Combat_Hit;

        _animOverride.ApplyOverrides(clipOverrides);

        clipVFXOverrides["Unit_Combat_VFX_Idle"] = info.Combat_VFX_Idle;
        clipVFXOverrides["Unit_Combat_VFX_Play"] = info.Combat_VFX_Play;

        _animVFXOverride.ApplyOverrides(clipVFXOverrides);
    }

    public void UpdateValues(bool flatanim)
    {
        healthbar.SetValue((float)unit.GetHealth() / (float)unit.GetMaxHealth(), flatanim);
    }

    public Unit GetUnit()
    {
        return unit;
    }

    public void DoAttackAnimation()
    {
        _anim.SetTrigger("Attack");
    }

    public void DoCounterAttackAnimation()
    {
        _anim.SetTrigger("Attack");
    }

    public void SetLive(bool isAlive)
    {
        _anim.SetBool("Live", isAlive);
    }

    public void PlayVFX()
    {
        _animVFX.SetTrigger("Play");
    }

    public void Hit()
    {
        _anim.SetTrigger("Hit");
    }

    public void setRangePositions(Unit_Info.UnitFireType fireType)
    {
        switch(fireType)
        {
            case Unit_Info.UnitFireType.MEELE:
                transform.position = MeelePos.position;
                break;
            case Unit_Info.UnitFireType.RANGED:
                transform.position = RangePos.position;
                break;
        }
    }

    public void SetOffetPivot(CombatScene_UnitAnimationEvents.Combat_Animation state)
    {
        UnitSpriteOffset.transform.localPosition = unit.GetUnitCombatPivot(state);
    }
}
