﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorManager : Manager
{
    public Cursor cursor; //NOTE we must to make it private
    public WorldMap Map;
    public Vector2 WorldSize; //Tentative... waiting for a file    

    protected override void Awake()
    {
        base.Awake();

        if(User.Instance)
        {
            // load inventory units
            // load recipes
        }

        // TODO: this function is duplicated form game manager
        CreateWorldMap(); 
        CreateCursor();
    }

    void CreateWorldMap()
    {
        Map = Instantiate(Map);
        Map.gameObject.layer = (int)SceneLayers.Scene_Map;
        Map.transform.SetParent(GameObject.Find("_Scene").transform);
        Map.name = Map.name.Split('(')[0];
        Map.SetWorldSize(new Vector2(WorldSize.y, WorldSize.x));
        Map.gameObject.SetActive(true);
    }

    void CreateCursor()
    {
        cursor = Instantiate(cursor);
        cursor.gameObject.layer = (int)SceneLayers.Scene_Map;
        cursor.transform.SetParent(GameObject.Find("_GameUI").transform);
        cursor.name = cursor.name.Split('(')[0];
        cursor.GetComponent<SpriteRenderer>().sortingLayerName = DrawGameLayers.MapCursor.ToString();
    }

}
