﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolSystem : MonoBehaviour {

    public int capacity = 20;
    public GameObject prefab;
    List<GameObject> pool;
    public bool willGrowCapacity;

	// Use this for initialization
	void Start ()
    {
		if(pool != null)
            foreach (GameObject obj in pool)
                Destroy(obj);

        pool = new List<GameObject>();
        for(int i = 0; i < capacity; i++)
        {
            GameObject obj = Instantiate(prefab);
            obj.layer = (int)Manager.SceneLayers.Scene_Map;
            obj.transform.SetParent(transform);
            obj.SetActive(false);
            pool.Add(obj);
        }
	}

    public GameObject Spawn()
    {
        GameObject obj = GetPooledObject();

        if (obj == null) return null;
        obj.SetActive(true);
        return obj;
    }

    public void ResetPool()
    {
        foreach (GameObject obj in pool)
            if (obj)
                obj.SetActive(false);
    }

    public List<GameObject> GetPoolActive()
    {
        List<GameObject> PoolActive = new List<GameObject>();
        foreach(GameObject obj in pool)
        {
            if (obj.activeInHierarchy)
                PoolActive.Add(obj);
        }
        return PoolActive;
    }

    GameObject GetPooledObject()
    {
        for (int i = 0; i < pool.Count; i++)
        {
            if (!pool[i].activeInHierarchy)
            {
                return pool[i];
            }
        }

        if (willGrowCapacity)
        {
            GameObject obj = Instantiate(prefab);
            obj.layer = (int)GameManager.SceneLayers.Scene_Map;
            obj.transform.SetParent(transform);
            obj.SetActive(false);
            pool.Add(obj);
            return obj;
        }

        return null;
    }
}
