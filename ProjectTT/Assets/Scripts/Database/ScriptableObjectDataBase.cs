﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
#if UNITY_EDITOR
    using UnityEditor;
#endif

public class ScriptableObjectDataBase<T> : ScriptableObject where T : class
{
    [SerializeField] List<T> database = new List<T>();
    [SerializeField] int idCounter;

    public void Add(T item)
    {
        if (item.GetType().GetField("Id") == null)
        {
            Debug.LogError("To add a new element, we need add a Id(int) field in our info class.");
            return;
        }

        System.Reflection.FieldInfo f = item.GetType().GetField("Id");

        if (f == null)
        {
            Debug.LogError("To add a new element, we need add a Id(int) field in our info class.");
            return;
        }

        System.Type t = f.FieldType;

        if (t != typeof(int))
        {
            Debug.LogError("To add a new element, we need add a Id(int) field in our info class.");
            return;
        }

        f.SetValue(item, idCounter);

        idCounter++;
        database.Add(item);
#if UNITY_EDITOR
        EditorUtility.SetDirty(this);
#endif
    }

    public void Insert(int index, T item)
    {
        if (item.GetType().GetField("Id") == null)
            return;

        System.Reflection.FieldInfo f = item.GetType().GetField("Id");

        if (f == null)
            return;

        System.Type t = f.FieldType;

        if (t != typeof(int))
            return;

        f.SetValue(item, idCounter);

        idCounter++;
        database.Insert(index, item);
#if UNITY_EDITOR
        EditorUtility.SetDirty(this);
#endif
    }

    public void Remove(T item)
    {
        database.Remove(item);
#if UNITY_EDITOR
        EditorUtility.SetDirty(this);
#endif
    }

    public void Remove(int index)
    {
        database.RemoveAt(index);
#if UNITY_EDITOR
        EditorUtility.SetDirty(this);
#endif
    }

    public void RemoveById(int Id)
    {
        if (database[0].GetType().GetField("Id") == null)
        {
            Debug.LogError("To add a new element, we need add a Id(int) field in our info class.");
            return;
        }

        System.Reflection.FieldInfo f = database[0].GetType().GetField("Id");

        if (f == null)
        {
            Debug.LogError("To add a new element, we need add a Id(int) field in our info class.");
            return;
        }

        System.Type t = f.FieldType;

        if (t != typeof(int))
        {
            Debug.LogError("To add a new element, we need add a Id(int) field in our info class.");
            return;
        }

        for (int i = 0; i < database.Count; i++ )
        {
            if ((int)f.GetValue(database[i]) == Id)
            {
                database.RemoveAt(i);
#if UNITY_EDITOR
                EditorUtility.SetDirty(this);
#endif
            return;
            }
        }

        Debug.LogWarning("There is no an item with this ID");
    }

    public int Count
    {
        get { return database.Count; }
    }

    public T GetItembyIndex(int index)
    {
        return database.ElementAt(index);
    }

    public T GetItembyId(int Id)
    {
        if (database[0].GetType().GetField("Id") == null)
        {
            Debug.LogError("To add a new element, we need add a Id(int) field in our info class.");
            return null;
        }

        System.Reflection.FieldInfo f = database[0].GetType().GetField("Id");

        if (f == null)
        {
            Debug.LogError("To add a new element, we need add a Id(int) field in our info class.");
            return null;
        }

        System.Type t = f.FieldType;

        if (t != typeof(int))
        {
            Debug.LogError("To add a new element, we need add a Id(int) field in our info class.");
            return null;
        }
        /////////////////////////////////////////////////////////////
        // Se hace la busqueda
        for (int i = 0; i < database.Count; i++)
        {
            if ((int)f.GetValue(database[i]) == Id)
                return database[i];
        }

        Debug.LogWarning("There is no an item with this ID");
        return null;
    }

    public T GetRandom()
    {
        int index = Random.Range(0, database.Count);
        return database.ElementAt(index);
    }

    public void Replace(int index, T item)
    {
        database[index] = item;
#if UNITY_EDITOR
        EditorUtility.SetDirty(this);
#endif
    }

    public bool UpdateItem(int Id, T item)
    {
        if (database[0].GetType().GetField("Id") == null)
        {
            Debug.LogError("To add a new element, we need add a Id(int) field in our info class.");
            return false;
        }

        System.Reflection.FieldInfo f = database[0].GetType().GetField("Id");

        if (f == null)
        {
            Debug.LogError("To add a new element, we need add a Id(int) field in our info class.");
            return false;
        }

        System.Type t = f.FieldType;

        if (t != typeof(int))
        {
            Debug.LogError("To add a new element, we need add a Id(int) field in our info class.");
            return false;
        }

        for (int i = 0; i < database.Count; i++)
        {
            if ((int)f.GetValue(database[i]) == Id)
            {
                database[i] = item;
#if UNITY_EDITOR
                EditorUtility.SetDirty(this);
#endif
                return true;
            }
        }

        Debug.LogWarning("There is no an item with this ID");
        return false;
    }

#if UNITY_EDITOR
    public U GetDataBase<U>(string dbPath, string dbName) where U : ScriptableObject
    {
        string dbFullpath = @"Assets/" + dbPath + "/" + dbName;

        U db = AssetDatabase.LoadAssetAtPath(dbFullpath, typeof(U)) as U;
        if (db == null)
        {
            if (!AssetDatabase.IsValidFolder("Assets/" + dbPath))
                AssetDatabase.CreateFolder("Assets", dbPath);

            db = CreateInstance<U>();
            AssetDatabase.CreateAsset(db, dbFullpath);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        return db;
    }
#endif
}
