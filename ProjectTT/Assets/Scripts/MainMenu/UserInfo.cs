﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UserInfo : MonoBehaviour {

    public Text UserName_Lbl;
    public Text UserLevel_Lbl;
    public Text UserGold_Lbl;

    // Use this for initialization
    void Start () {
		if (User.Instance)
        {
            UserName_Lbl.text = "Name: " + User.Instance.GetUserName();
            UserLevel_Lbl.text = "Level: " + User.Instance.GetUserLevel();
            UserGold_Lbl.text = "Gold: " + User.Instance.GetUserGold();
        }
	}
}
