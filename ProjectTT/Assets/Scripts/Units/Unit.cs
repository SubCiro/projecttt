﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[System.Serializable]
public class Unit_Info
{
    public enum UnitType
    {
        Summoner    = 0,                                            // hero (Player Represented).
        Minion      = 1,                                            // Minions controled by Player.
    }; // Unit types    

    public enum UnitFireType
    {
        MEELE,
        RANGED
    } // Types of fire

    //Info
    public int Id;                                                  // Numerical identifier
    public string Name;                                             // This Unit Type
    public string Description;                                      // Lore
    public UnitType Type;                                           // If this unit is a Summoner or Minion

    //Player Exclusive Stats
    public int Energy;                                              // Current energy to summon other minions
    public int MaxEnergy;                                           // Max Energy

    public int EnergyCost;                                          // Just for minions,cost of energy's player for be summoned

    // STATUS
    public int Health = 10;                                         // This Unit Heatlh
    public int MaxHealth;   

    //MOVEMENT
    public int Move;                                                // Movement speed of this unit    
    public List<WorldMap_Cell_Info.TerrainType> ImpassableCells;    // List of terrain types whose this unit SHALL NOT PASS

    //VISION
    public int Vision;                                              // Amount os Vision Distance that this unit can see
    public bool Visible;                                            // if the unit is currently showed or not

    //COMBAT
    public UnitFireType FireType;                                   // Unit Fire Type
    public int Attack;                                               // Attack Damage of this unit
    public int Defense;                                             // Defense of this unit
    //public List<UnitType> Advantage_Types;                          // List of Units types whose this unit had avantage
    //public List<UnitType> Disdvantage_Types;                        // List of Units types whose this unit had disavantage
    public int[] FireRange;                                         // JUST FOR RAGNED UNITS

    // BONUS STATUS
    public int BONUS_Attack;                                         // BONUS Attack Damage of this unit
    public int BONUS_Defense;                                       // BONUS Defense of this unit
    public int BONUS_Move;                                          // BONUS Movement speed of this unit  
    public int BONUS_Vision;                                        // BONUS Vision for this unit

    //Animations WorldView
    public AnimationClip IdleReady;
    public AnimationClip IdleResting;
    public AnimationClip Dying;
    public AnimationClip WalkUp;
    public AnimationClip WalkDown;
    public AnimationClip WalkSide;

    //Animations CombatView 
    public AnimationClip Combat_Idle;
    public AnimationClip Combat_Dying;
    public AnimationClip Combat_Attack;
    public AnimationClip Combat_Defend;
    public AnimationClip Combat_Hit;
    public AnimationClip Combat_VFX_Idle;
    public AnimationClip Combat_VFX_Play;

    // Pivots
    public Vector2 Pvt_IdleReady;
    public Vector2 Pvt_IdleResting;
    public Vector2 Pvt_Dying;
    public Vector2 Pvt_WalkUp;
    public Vector2 Pvt_WalkDown;
    public Vector2 Pvt_WalkSide;

    public Vector2 Pvt_Combat_Idle;
    public Vector2 Pvt_Combat_Dying;
    public Vector2 Pvt_Combat_Attack;
    public Vector2 Pvt_Combat_Defend;
    public Vector2 Pvt_Combat_Hit;
    public Vector2 Pvt_Combat_VFX_Idle;
    public Vector2 Pvt_Combat_VFX_Play;

    // set from an existing unit (Dictionary)
    public Unit_Info(Unit_Info info)
    {
        Id                  = info.Id;
        Name                = info.Name;
        Description         = info.Description;        
        Type                = info.Type;

        Energy              = info.Energy;
        MaxEnergy           = info.MaxEnergy;

        EnergyCost          = info.EnergyCost;

        Health              = info.Health;
        MaxHealth           = info.MaxHealth;
        Move                = info.Move;
        Vision              = info.Vision;
        Attack               = info.Attack;
        Defense             = info.Defense;
        //Advantage_Types     = ;
        //Disdvantage_Types   = ;
        ImpassableCells     = info.ImpassableCells;
        FireType            = info.FireType;
        FireRange           = info.FireRange;

        //Animations WorldView
        IdleReady           = info.IdleReady;
        IdleResting         = info.IdleResting;
        Dying               = info.Dying;
        WalkUp              = info.WalkUp;
        WalkDown            = info.WalkDown;
        WalkSide            = info.WalkSide;

        //Animations CombatView
        Combat_Idle         = info.Combat_Idle;
        Combat_Dying        = info.Combat_Dying;
        Combat_Attack       = info.Combat_Attack;
        Combat_Defend       = info.Combat_Defend;
        Combat_Hit          = info.Combat_Hit;

        Combat_VFX_Idle     = info.Combat_VFX_Idle;
        Combat_VFX_Play     = info.Combat_VFX_Play;


        // Pivots
        Pvt_IdleReady       = info.Pvt_IdleReady;
        Pvt_IdleResting     = info.Pvt_IdleResting;
        Pvt_Dying           = info.Pvt_Dying;
        Pvt_WalkUp          = info.Pvt_WalkUp;
        Pvt_WalkDown        = info.Pvt_WalkDown;
        Pvt_WalkSide        = info.Pvt_WalkSide;

        Pvt_Combat_Idle     = info.Pvt_Combat_Idle;
        Pvt_Combat_Dying    = info.Pvt_Combat_Dying;
        Pvt_Combat_Attack   = info.Pvt_Combat_Attack;
        Pvt_Combat_Defend   = info.Pvt_Combat_Defend;
        Pvt_Combat_Hit      = info.Pvt_Combat_Hit;
        Pvt_Combat_VFX_Idle = info.Pvt_Combat_VFX_Idle;
        Pvt_Combat_VFX_Play = info.Pvt_Combat_VFX_Play;
    }

    // set Defaut values as a Minion *GENERIC CONSTRUCTOR*
    public Unit_Info()
    {
        Id                  = -1; //edit this
        Name                = "New Minion";
        Description         = "A new unit just created";  
        
        MaxEnergy           = 200;
        Energy              = MaxEnergy;

        EnergyCost          = 50;

        Type                = UnitType.Minion;
        MaxHealth           = 10;
        Health              = MaxHealth;        
        Move                = 3;
        Vision              = 2;
        Attack               = 5;
        Defense             = 5;
        //Advantage_Types     = Advantage_Catalog         [(int)Type];
        //Disdvantage_Types   = Disadvantage_Catalog      [(int)Type];
        ImpassableCells     = new List<WorldMap_Cell_Info.TerrainType>();
        FireType            = UnitFireType.MEELE;
        FireRange           = new int[2];

        //Animations WorldView
        IdleReady           = null;
        IdleResting         = null;
        Dying               = null;
        WalkUp              = null;
        WalkDown            = null;
        WalkSide            = null;

        //Animations CombatView
        Combat_Idle         = null;
        Combat_Dying        = null;
        Combat_Attack       = null;
        Combat_Defend       = null;
        Combat_Hit          = null;

        Combat_VFX_Idle = null;
        Combat_VFX_Play = null;

        // Pivots
        Pvt_IdleReady = new Vector2();
        Pvt_IdleResting = new Vector2();
        Pvt_Dying = new Vector2();
        Pvt_WalkUp = new Vector2();
        Pvt_WalkDown = new Vector2();
        Pvt_WalkSide = new Vector2();

        Pvt_Combat_Idle = new Vector2();
        Pvt_Combat_Dying = new Vector2();
        Pvt_Combat_Attack = new Vector2();
        Pvt_Combat_Defend = new Vector2();
        Pvt_Combat_Hit = new Vector2();
        Pvt_Combat_VFX_Idle = new Vector2();
        Pvt_Combat_VFX_Play = new Vector2();
    }    
}

public class Unit : MonoBehaviour {
    public enum MovementDirection { North, Sounth, East, Weast }
    public enum UnitState
    {
        READY = 0,
        USED = 1,
        SELECTED = 2,
        ONTRASIT = 3,
        ONATTACKORDER = 4,
        ONATTACKING = 5,
        ONDYING = 6
    } // units states or phases

    WorldMap_Cell currentCell;                               // Current Position
    Player OwnerPlayer;                                      // Owner Player
    UnitState State;   
    MovementDirection movementDirection;
    Unit_Info info;
    List<WorldMap_Cell> Path;
    List<WorldMap_Cell> AttackableCells;
    public float Velocity = 5;

    Text healthLabel;
    Image healthBackground;
    SpriteRenderer sprite;
    bool Surprised;
    UnitAnimator _anim;
    
    public void Init()
    {
        info = new Unit_Info();
        GetComponentInChildren<SpriteRenderer>().sortingLayerName = Manager.DrawGameLayers.Unit.ToString();
        GetComponentInChildren<Canvas>().sortingLayerName = Manager.DrawGameLayers.Unit.ToString();

        healthLabel = GetComponentInChildren<Text>();
        healthBackground = GetComponentInChildren<Image>();
        sprite = GetComponentInChildren<SpriteRenderer>();
        _anim = GetComponent<UnitAnimator>();
        UpdateHealthLabel();
    }
    public void Init(int ID)
    {        
        info = new Unit_Info(GameManager.Instance.unitDatabase.GetItembyId(ID));
        name = info.Name;
        GetComponentInChildren<SpriteRenderer>().sortingLayerName = Manager.DrawGameLayers.Unit.ToString();
        GetComponentInChildren<Canvas>().sortingLayerName = Manager.DrawGameLayers.Unit.ToString();

        healthLabel = GetComponentInChildren<Text>();
        healthBackground = GetComponentInChildren<Image>();
        sprite = GetComponentInChildren<SpriteRenderer>();
        _anim = GetComponent<UnitAnimator>();
        UpdateHealthLabel();
    }
    public void InitRandom()    //Exclusive to minions for now
    {
        info = new Unit_Info(GameManager.Instance.unitDatabase.GetRandom());
        while (GetUnitType() == Unit_Info.UnitType.Summoner)
        {
            info = new Unit_Info(GameManager.Instance.unitDatabase.GetRandom());
        }
        name = info.Name;
        GetComponentInChildren<SpriteRenderer>().sortingLayerName = Manager.DrawGameLayers.Unit.ToString();
        GetComponentInChildren<Canvas>().sortingLayerName = Manager.DrawGameLayers.Unit.ToString();

        healthLabel = GetComponentInChildren<Text>();
        healthBackground = GetComponentInChildren<Image>();
        sprite = GetComponentInChildren<SpriteRenderer>();
        _anim = GetComponent<UnitAnimator>();
        UpdateHealthLabel();
    }
    public void UpdateWorldAnimationsClips()
    {
        _anim.UpdateWorldAnimationsClips(info);
    }
    public void UpdateCombatAnimationsClips(CombatScene_Unit combatUnit)
    {
        combatUnit.UpdateCombatAnimationsClips(info);
    }

    public Unit_Info.UnitType GetUnitType()
    {
        return info.Type;
    }
    public UnitState GetState()
    {
        return State;
    }
    public Vector2 GetUnitMapPivot(UnitState state)
    {
        switch(state)
        {
            case UnitState.READY:
            case UnitState.SELECTED:
            case UnitState.ONATTACKORDER:
            case UnitState.ONATTACKING:
                return info.Pvt_IdleReady;
            case UnitState.USED:
                return info.Pvt_IdleResting;
            case UnitState.ONTRASIT:
                return info.Pvt_WalkSide;
            case UnitState.ONDYING:
                return info.Pvt_Dying;
            default:
                return Vector2.zero;
        }
    }
    public Vector2 GetUnitCombatPivot(CombatScene_UnitAnimationEvents.Combat_Animation state)
    {
        switch (state)
        {
            case CombatScene_UnitAnimationEvents.Combat_Animation.ATTACK:
                return info.Pvt_Combat_Attack;
            case CombatScene_UnitAnimationEvents.Combat_Animation.DEFEND:
                return info.Pvt_Combat_Defend;
            case CombatScene_UnitAnimationEvents.Combat_Animation.DYING:
                return info.Pvt_Combat_Dying;
            case CombatScene_UnitAnimationEvents.Combat_Animation.HIT:
                return info.Pvt_Combat_Hit;
            case CombatScene_UnitAnimationEvents.Combat_Animation.IDLE:
                return info.Pvt_Combat_Idle;
            default:
                return Vector2.zero;
        }
    }
    public void SetUnitState(UnitState state)
    {
        State = state;
    }

    public WorldMap_Cell GetCurrentPosition()
    {
        return currentCell;
    }
    public void SetCurrentPosition(WorldMap_Cell cell) //tentative for pathfinding services
    {
        currentCell = cell;
        SetOrderInlayer(GetCurrentPosition().GetOrderedinLayer());
        transform.position = currentCell.transform.position;
    }

    public List<WorldMap_Cell_Info.TerrainType> GetImpassableCells()
    {
        return info.ImpassableCells;
    }

    public Player GetOwnerPlayer()
    {
        return OwnerPlayer;
    }
    public void SetOwnerPlayer(Player player)
    {
        OwnerPlayer = player;
        healthBackground.color = player.PlayerColor;
    }

    public int GetHealth()
    {
        return info.Health;
    }
    public int GetMaxHealth()
    {
        return info.MaxHealth;
    }

    public void Move()
    {
        OwnerPlayer.OnUnitMenu();
        SetUnitState(UnitState.ONTRASIT);
        GameManager.Instance.Pathfinder.ResetNavMesh();
        GameManager.Instance.Pathfinder.ResetPathMesh();

        StartCoroutine(MoveAnimation(Path));
    }   //move order

    IEnumerator MoveAnimation(List<WorldMap_Cell> Path)
    {
        for (int i = Path.Count - 1; i >= 0; i--)
        {
            while (Vector2.Distance(transform.position, Path[i].transform.position) > 0.01f)
            {
                transform.position = Vector2.MoveTowards(transform.position, Path[i].transform.position, Velocity * Time.fixedDeltaTime);

                Vector2 Direction = (Path[i].transform.position - transform.position).normalized;

                     if (Direction.x > 0 && Direction.y > 0) // North-East
                    movementDirection = MovementDirection.North;
                else if (Direction.x < 0 && Direction.y < 0) // South-Weats
                    movementDirection = MovementDirection.Sounth;
                else if (Direction.x > 0 && Direction.y < 0) // North-Weats
                    movementDirection = MovementDirection.North;
                else if (Direction.x < 0 && Direction.y > 0) // South-East
                    movementDirection = MovementDirection.Sounth;
                else if (Direction.x > 0 && Direction.y == 0) // East
                    movementDirection = MovementDirection.East;
                else if (Direction.x < 0 && Direction.y == 0) // Sounth
                    movementDirection = MovementDirection.Weast;
                else if (Direction.x == 0 && Direction.y > 0) // East
                    movementDirection = MovementDirection.North;
                else if (Direction.x == 0 && Direction.y < 0) // Weast
                    movementDirection = MovementDirection.Sounth;

                yield return null;
            }

            SetOrderInlayer(Path[i].GetOrderedinLayer());
            transform.position = Path[i].transform.position;

            //when your path cross by an enemy unit
            if (i - 1 >= 0)
                if (Path[i - 1].GetUnit())
                    if (Path[i - 1].GetUnit().GetOwnerPlayer() != GetOwnerPlayer())
                    {
                        //SURPRICED!!
                        GameManager.Instance.ShakeCameras(.1f, .1f);
                        Surprised = true;
                        Path.RemoveRange(0, i);
                        break;
                    }
        }

        if (!Surprised)
        {
            SetUnitState(UnitState.SELECTED);

            //Can Attack afterMove OPTIMIZE THIS!!
            switch (info.FireType)
            {
                case Unit_Info.UnitFireType.MEELE:
                    AttackableCells = GameManager.Instance.Pathfinder.GetCellsRange(Path[0], 1, Pathfinder.NavFilter.ATTACK_VISION | Pathfinder.NavFilter.NOT_ALLIES | Pathfinder.NavFilter.NOT_EMPTY_UNIT | Pathfinder.NavFilter.NOT_ORIGIN, -1);
                    
                    //filtering options of unit
                    if (GetAtacableCellsCount() > 0)
                    {
                        GameManager.Instance.UnitsMenuApplyFilters(UnitOption.UnitOptionFilters.CAN_REST | UnitOption.UnitOptionFilters.CAN_ATTACK);
                    }
                    else if (GetAtacableCellsCount() == 0)
                    {
                        GameManager.Instance.UnitsMenuApplyFilters(UnitOption.UnitOptionFilters.CAN_REST);
                    }
                    GameManager.Instance.UnitsMenuEnable(true);
                    break;
                case Unit_Info.UnitFireType.RANGED:
                    if (Path[0] == GetCurrentPosition())
                    {
                        AttackableCells = GameManager.Instance.Pathfinder.GetCellsRange(Path[0], info.FireRange[0], info.FireRange[1], Pathfinder.NavFilter.ATTACK_VISION | Pathfinder.NavFilter.NOT_ALLIES | Pathfinder.NavFilter.NOT_EMPTY_UNIT | Pathfinder.NavFilter.NOT_ORIGIN, -1);
                        

                        //filtering options of unit
                        if (GetAtacableCellsCount() > 0)
                        {
                            GameManager.Instance.UnitsMenuApplyFilters(UnitOption.UnitOptionFilters.CAN_REST | UnitOption.UnitOptionFilters.CAN_ATTACK);
                        }
                        else if (GetAtacableCellsCount() == 0)
                        {
                            GameManager.Instance.UnitsMenuApplyFilters(UnitOption.UnitOptionFilters.CAN_REST);
                        }
                        GameManager.Instance.UnitsMenuEnable(true);
                    }
                    else
                    {
                        
                        GameManager.Instance.UnitsMenuApplyFilters(UnitOption.UnitOptionFilters.CAN_REST);
                        GameManager.Instance.UnitsMenuEnable(true);
                    }
                    break;
            }
        }
        else
        {
            GetOwnerPlayer().RestUnitAfterAction(this);
        }
    }    //Move animation

    public MovementDirection GetMuvementDirection()
    { return movementDirection; }

    public void Select()
    {
        SetUnitState(UnitState.SELECTED);
        LoadTansitableCells();
        LoadPathToMove(GameManager.Instance.cursor.getTargetCell());
    }

    public void ChangeDestination()
    {
        LoadPathToMove(GameManager.Instance.cursor.getTargetCell());
    }

    public void Ready()
    {
        SetUnitState(UnitState.READY);        
        Surprised = false;
    }

    public void RestAfterAction()
    {
        //set of new position
        GetCurrentPosition().SetUnit(null);
        Path[0].SetUnit(this);

        //fog of war
        foreach (WorldMap_Cell tc in Path)
        {
            tc.SetTransited(true, info.Vision);
        }

        //FogofWarUpdate
        if (GameManager.Instance.Map.FogOfWar)        
            GameManager.Instance.Map.UpdateFog();
        //
        SetUnitState(UnitState.USED);
    }

    public void LoadTansitableCells()
    {
        List<WorldMap_Cell> TransitableCells = GameManager.Instance.Pathfinder.GetCellsRange(GetCurrentPosition(), info.Move, Pathfinder.NavFilter.MOVEMENT | Pathfinder.NavFilter.IMPASSABLE_CELLS | Pathfinder.NavFilter.NOT_ENEMIES, 1);
        GameManager.Instance.Pathfinder.LoadNavMesh(TransitableCells);
    }

    public void LoadPathToMove(WorldMap_Cell Destination) 
    {
        Path = GameManager.Instance.Pathfinder.CalculatePath(GetCurrentPosition(), Destination, info.Move);
        GameManager.Instance.Pathfinder.LoadPathMesh(Path);
    }//load path to move, this is going to tranform to a more general usage, not just movement

    public void AttackingOrder()
    {
        SetUnitState(UnitState.ONATTACKORDER);        
        GameManager.Instance.Pathfinder.LoadFireRange(AttackableCells);
        GameManager.Instance.SetCursorLocation(AttackableCells[0]);
    }

    public WorldMap_Cell GetDestination()
    {
        return Path[0];
    }

    public int GetAtacableCellsCount()
    {
        if (GetUnitType() != Unit_Info.UnitType.Summoner)
            return AttackableCells.Count;
        else
            return 0;
    }

    public void TargetUnitToAttack(Pathfinder.Directions4 direction)
    {
        WorldMap_Cell newAttackingTarget = GameManager.Instance.Pathfinder.DirectionalSearch(GameManager.Instance.GetCursorLocation(), AttackableCells, direction);
        if(newAttackingTarget)
            GameManager.Instance.SetCursorLocation(newAttackingTarget);
    }

    public void AttackUnit(Unit unit)
    {        
        unit.GetDamage(info.BONUS_Attack + (info.Attack), false);
    }
    public void CounterAttackUnit(Unit unit)
    {
        //Counter Damage Calculation
        unit.GetDamage((info.BONUS_Attack + (info.Attack)) / 2, true);
    }

    public void FireRangeCheckOn()
    {
        switch(info.FireType)
        {
            case Unit_Info.UnitFireType.MEELE:
                AttackableCells = GameManager.Instance.Pathfinder.GetCellsRange(GetCurrentPosition(), info.Move, Pathfinder.NavFilter.MOVEMENT | Pathfinder.NavFilter.IMPASSABLE_CELLS | Pathfinder.NavFilter.NOT_ENEMIES, 1);
                AttackableCells = GameManager.Instance.Pathfinder.ExpandRange(AttackableCells, 1);
                break;
            case Unit_Info.UnitFireType.RANGED:
                AttackableCells = GameManager.Instance.Pathfinder.GetCellsRange(GetCurrentPosition(), info.FireRange[0], info.FireRange[1], Pathfinder.NavFilter.ATTACK_VISION | Pathfinder.NavFilter.NOT_ORIGIN, 0);
                break;
        }
        GameManager.Instance.Pathfinder.LoadFireRange(AttackableCells);
    }
    public void FireRangeCheckOff()
    {
        GameManager.Instance.Pathfinder.ResetFireCells();
    }

    public void GetDamage(int Damage, bool isCounter)
    {        
        GameManager.Instance.ShakeCameras(.1f, .2f);

        if(!isCounter)
            Damage -= ((info.BONUS_Defense + info.Defense) / 2);
        else
            Damage -= ((info.BONUS_Defense + info.Defense) / 3);

        Debug.Log(name + "receives " + Damage + " of Damage");

        if (Damage > 0)
            info.Health -= Damage;
        
        if (info.Health < 0)
            info.Health = 0;
        UpdateHealthLabel();
    }

    public void DestroyUnit()
    {
        GetOwnerPlayer().UnitDestroyed(this);
    }

    void UpdateHealthLabel()
    {
        healthLabel.text = "" + info.Health;
    }

    public Unit_Info.UnitFireType GetFireType()
    {
        return info.FireType;
    }

    public void SetVisible(bool Visible)
    {
        info.Visible = Visible;

        sprite.enabled = info.Visible;
        healthBackground.enabled = info.Visible;
        healthLabel.enabled = info.Visible;
    }

    public bool IsVisible()
    {
        return info.Visible;
    }

    public int GetVision()
    {
        return info.Vision;
    }

    public void SetOrderInlayer(int OrderInlayer)
    {
        sprite.sortingOrder = OrderInlayer;
    }
}
