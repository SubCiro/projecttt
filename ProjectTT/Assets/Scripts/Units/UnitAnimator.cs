﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitAnimator : MonoBehaviour {

    Animator _anim;
    AnimatorOverrideController _animOverride;
    protected AnimationClipOverrides clipOverrides;
    Unit unit;
    SpriteRenderer render;    

	// Use this for initialization
	void Start ()
    {
        _anim = GetComponentInChildren<Animator>();
        _animOverride = new AnimatorOverrideController(_anim.runtimeAnimatorController);
        _anim.runtimeAnimatorController = _animOverride;
        clipOverrides = new AnimationClipOverrides(_animOverride.overridesCount);
        _animOverride.GetOverrides(clipOverrides);

        unit = GetComponent<Unit>();
        render = GetComponentInChildren<SpriteRenderer>();
        unit.UpdateWorldAnimationsClips();
	}

    public void UpdateWorldAnimationsClips(Unit_Info info)
    {
        clipOverrides["Unit_IdleReady"] = info.IdleReady;
        clipOverrides["Unit_IdleResting"] = info.IdleResting;
        clipOverrides["Unit_Die"] = info.Dying;
        clipOverrides["Unit_WalkUp"] = info.WalkUp;
        clipOverrides["Unit_WalkDown"] = info.WalkDown;
        clipOverrides["Unit_WalkSide"] = info.WalkSide;
        _animOverride.ApplyOverrides(clipOverrides);
    }
	
	// Update is called once per frame
	void Update () {
		switch(unit.GetState())
        {
            case Unit.UnitState.READY:
            case Unit.UnitState.SELECTED:
                render.color = Color.white;
                //render.flipX = false;
                _anim.SetLayerWeight(1, 0);
                _anim.SetInteger("State", 0);
                break;
            case Unit.UnitState.USED:
                render.color = Color.gray;
                //render.flipX = false;
                _anim.SetLayerWeight(1, 0);
                _anim.SetInteger("State", 1);
                break;
            case Unit.UnitState.ONTRASIT:            
                _anim.SetLayerWeight(1, 1);
                switch (unit.GetMuvementDirection())
                {
                    case Unit.MovementDirection.North:
                        //render.flipX = false;
                        _anim.SetInteger("Direction", 0);
                        break;
                    case Unit.MovementDirection.East:
                        //render.flipX = true;
                        _anim.SetInteger("Direction", 1);
                        break;
                    case Unit.MovementDirection.Weast:
                        //render.flipX = false;
                        _anim.SetInteger("Direction", 1);
                        break;
                    case Unit.MovementDirection.Sounth:
                        //render.flipX = false;
                        _anim.SetInteger("Direction", 2);
                        break;
                }                
                break;
            case Unit.UnitState.ONDYING:
                //render.flipX = false;
                _anim.SetLayerWeight(1, 0);
                _anim.SetInteger("State", 2);
                break;
        }

        render.transform.localPosition = unit.GetUnitMapPivot(unit.GetState());
    }

    public void DeathAnimetionEndEvent()
    {
        unit.DestroyUnit();
    }
}
