﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitAnimationEvents : MonoBehaviour {

    UnitAnimator animator;

    private void Awake()
    {
        animator = transform.parent.GetComponent<UnitAnimator>();
    }

    public void DeathAnimetionEndEvent()
    {
        animator.DeathAnimetionEndEvent();
    }
}
