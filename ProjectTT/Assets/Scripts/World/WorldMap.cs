﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldMap : MonoBehaviour {

    public static WorldMap Instance;

    public WorldMap_Cell CellPrefab;                //Cell Prefab
    public List<List<WorldMap_Cell>> WorldCells;    //WorldMap Matrix

    Vector2 WorldSize;                              // World Size

    public void SetWorldSize(Vector2 Size) { WorldSize = Size; }    //Set World Size
    public Vector2 GetWorldSize() { return WorldSize; }             //Get World Size
    public Vector2 OffsetTilePosition;

    BoxCollider2D WorldBounds;

    //Fog Of War
    public bool FogOfWar = false; //boolean whose enables/disables fog of war calculations in game **EDITABLE
    bool LastFogOfWar = false;

    #region INITIALIZE_WORLDMAP
    void Awake()
    {
        //set Global Instance
        Instance = this;
        WorldBounds = GetComponent<BoxCollider2D>();

        //Create Map
        CreateWorldMap();

        //set Map Navigations
        SetMapNavigations();
                
    }

    void CreateWorldMap() // no file yet hehe :c
    {
        //getting sprite's size of the WorldCell
        Vector2 spriteSize = new Vector2(CellPrefab.GetComponent<SpriteRenderer>().bounds.size.x, CellPrefab.GetComponent<SpriteRenderer>().bounds.size.y);

        // this vector2 controls if our mapsize axis is even, if this is true... then we need to centralize our map!!
        Vector2 Pair_Reduction = Vector2.zero;
        if ((WorldSize.x % 2) == 0)
            Pair_Reduction.x = spriteSize.x / 2;
        if ((WorldSize.y % 2) == 0)
            Pair_Reduction.y = spriteSize.y / 2;

        WorldBounds.size = new Vector2(WorldSize.y * (spriteSize.x), WorldSize.x * (spriteSize.y));

        if (GameManager.Instance)
            GameManager.Instance.SceneMap_Cam_Movement.SetCameraBounds(WorldBounds.bounds);

        //initialize our mapsettings
        WorldCells = new List<List<WorldMap_Cell>>();
        for (int i = 0; i < WorldSize.x; i++)
        {
            List<WorldMap_Cell> Row = new List<WorldMap_Cell>();
            for (int j = 0; j < WorldSize.y; j++)
            {
                Vector3 CellPosition = new Vector3((((j - Mathf.Floor(WorldSize.y / 2)) * spriteSize.x) + Pair_Reduction.y), (((i - Mathf.Floor(WorldSize.x / 2)) * spriteSize.y) + Pair_Reduction.x)); //NOTE: i'm inverting the coordinate system because set the position as cardinal points (fucking dump me);

                if (i > 0)
                    CellPosition = new Vector3(CellPosition.x, CellPosition.y + (OffsetTilePosition.x * i));

                //set Position
                WorldMap_Cell Cell = Instantiate(
                    CellPrefab,
                    CellPosition,
                    Quaternion.identity,
                    gameObject.transform);

                Cell.gameObject.layer = (int)Manager.SceneLayers.Scene_Map;

                //Set Name
                Cell.gameObject.name = "World_Cell_" + j + "_" + i;

                //Set Values
                Cell.Init(WorldMap_Cell_Info.TerrainType.Grass, new Vector2(j, i), -(j + i * (int)WorldSize.y)-1);

                //Add Cells to Row
                Row.Add(Cell);
            }
            //Add Rows to WorldMatrix
            WorldCells.Add(Row);
        }
    }

    void SetMapNavigations() // once worldmap created we need to link their neightbors
    {
        for (int i = 0; i < WorldSize.x; i++)
        {
            for (int j = 0; j < WorldSize.y; j++)
            {
                WorldCells[i][j].InitNavigation(WorldCells[i][j].GetPosition()); //REPLACE for get position!!
            }
        }
    }
    #endregion

    #region WORLDMAP_INFORMATION
    public WorldMap_Cell getWorldCell(Vector2 position) // get cell on that position //NOTE: i'm inverting the coordinate system because set the position as cardinal points (fucking dump) 
    {
        if (position.y >= WorldSize.x || position.x >= WorldSize.y || position.y <= -1 || position.x <= -1)
            return null;
        else
            return WorldCells[(int)position.y][(int)position.x];
    }

    public WorldMap_Cell getWorldCell(int x, int y) // get cell on that position
    {
        return WorldCells[x][y];
    }

    public Bounds GetBounds()
    {
        return WorldBounds.bounds;
    }
    #endregion

    private void Update()
    {
        if(FogOfWar != LastFogOfWar)
        {
            LastFogOfWar = FogOfWar;
            SetFog(FogOfWar);
        }
    }

    void SetFog(bool enable)
    {
        if (!enable)
        {
            //set visible all cells
            for (int i = 0; i < WorldSize.x; i++)
            {
                for (int j = 0; j < WorldSize.y; j++)
                {
                    WorldCells[i][j].SetFogCell(true);
                }
            }
        }
        else
        {
            //"clean"
            for (int i = 0; i < WorldSize.x; i++)
            {
                for (int j = 0; j < WorldSize.y; j++)
                {
                    WorldCells[i][j].SetFogCell(false);
                }
            }

                    //apply the fog
            for (int i = 0; i < WorldSize.x; i++)
            {
                for (int j = 0; j < WorldSize.y; j++)
                {
                    if (WorldCells[i][j].GetUnit())
                    {
                        if (WorldCells[i][j].GetUnit().GetOwnerPlayer().GetFogVision())
                        {
                            List<WorldMap_Cell> visiblecells = GameManager.Instance.Pathfinder.GetCellsRange(WorldCells[i][j], WorldCells[i][j].GetUnit().GetVision(), Pathfinder.NavFilter.ATTACK_VISION, 0);
                            foreach (WorldMap_Cell c in visiblecells)
                            {
                                c.SetFogCell(true);
                            }
                        }
                    }
                    else if (WorldCells[i][j].WasTransited())
                    {
                        List<WorldMap_Cell> visiblecells = GameManager.Instance.Pathfinder.GetCellsRange(WorldCells[i][j], WorldCells[i][j].GetRangeTransitedVision(), Pathfinder.NavFilter.ATTACK_VISION, 0);
                        foreach (WorldMap_Cell c in visiblecells)
                        {
                            c.SetFogCell(true);
                        }
                    }
                }
            }
        }
    }
    public void UpdateFog()
    {
        //"clean"
        for (int i = 0; i < WorldSize.x; i++)
        {
            for (int j = 0; j < WorldSize.y; j++)
            {
                WorldCells[i][j].SetFogCell(false);
            }
        }

        //apply the fog
        for (int i = 0; i < WorldSize.x; i++)
        {
            for (int j = 0; j < WorldSize.y; j++)
            {
                if (WorldCells[i][j].GetUnit())
                {
                    if (WorldCells[i][j].GetUnit().GetOwnerPlayer().GetFogVision())
                    {
                        List<WorldMap_Cell> visiblecells = GameManager.Instance.Pathfinder.GetCellsRange(WorldCells[i][j], WorldCells[i][j].GetUnit().GetVision(), Pathfinder.NavFilter.ATTACK_VISION, 0);
                        foreach (WorldMap_Cell c in visiblecells)
                        {
                            c.SetFogCell(true);
                        }
                    }
                }
                else if(WorldCells[i][j].WasTransited())
                {
                    List<WorldMap_Cell> visiblecells = GameManager.Instance.Pathfinder.GetCellsRange(WorldCells[i][j], WorldCells[i][j].GetRangeTransitedVision(), Pathfinder.NavFilter.ATTACK_VISION, 0);
                    foreach (WorldMap_Cell c in visiblecells)
                    {
                        c.SetFogCell(true);
                    }
                }
            }
        }
    }
    public void ResetTransitedCells()
    {
        for (int i = 0; i < WorldSize.x; i++)
        {
            for (int j = 0; j < WorldSize.y; j++)
            {
                WorldCells[i][j].SetTransited(false, 0);
            }
        }
    }


}
