﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class Pathfinder : MonoBehaviour { //change name for PathfinderServices or WorldCells Selection Services idk

    public enum NavFilter
    {
        MOVEMENT                = 0x00000001,
        IMPASSABLE_CELLS        = 0x00000010,
        NOT_ALLIES              = 0x00000100,
        NOT_ENEMIES             = 0x00001000,
        ATTACK_VISION           = 0x00010000,
        NOT_ORIGIN              = 0x00100000,
        NOT_EMPTY_UNIT          = 0x01000000               
    }

    public enum Directions4
    {
        NORTH,
        SOUNTH,
        EAST,
        WEAST
    }

    public PoolSystem NavCellPool;
    public PoolSystem TracerPathPool;
    public PoolSystem FireRangePool;

    private Pathfinder_Tracer Tracer = new Pathfinder_Tracer();
    
    // GENERAL SELECTION
    public List<WorldMap_Cell> GetCellsRange(WorldMap_Cell Origin, int Range, NavFilter filters, int AddRemoveFog)
    {
        bool AddNotHighLighted = false;
        List<WorldMap_Cell> Closed = new List<WorldMap_Cell>();
        List<Pathfinder_CellCheck> Open = new List<Pathfinder_CellCheck>();

        Open.Add(new Pathfinder_CellCheck(Origin, 0)); // there is not cost if you already be there

        Origin.SetHeuristic(0);

        while (Open.Count > 0)
        {
            Pathfinder_CellCheck currentCheck = Open[0];
            Open.RemoveAt(0);

            if (Closed.Contains(currentCheck.lastCell)) // if the final list already have the current checking one -> skip
                continue;

            if (currentCheck.cost > Range)  // if the current cost overcome the range -> skip
                continue;

            if (!currentCheck.lastCell.IsHighLighted() && currentCheck.lastCell != Origin)
                if (AddRemoveFog == -1)
                    continue;
                else if (AddRemoveFog == 1)
                    AddNotHighLighted = true;

            if(currentCheck.lastCell.GetUnit()) // if the current checking cell has a unit
            {
                if ((filters & NavFilter.NOT_ENEMIES) == NavFilter.NOT_ENEMIES) // pass if its enemy
                    if (currentCheck.lastCell.GetUnit().GetOwnerPlayer() != GameManager.Instance.GetPlayerCurrentTurn() && currentCheck.lastCell != Origin) //if the cheking cell has a unit that is not mine? -> skip
                        if(currentCheck.lastCell.IsHighLighted())
                            continue;
                        else
                            if(!AddNotHighLighted)
                            continue;

                if ((filters & NavFilter.NOT_ALLIES) == NavFilter.NOT_ALLIES) // pass if its ally
                    if (currentCheck.lastCell.GetUnit().GetOwnerPlayer() == GameManager.Instance.GetPlayerCurrentTurn() && currentCheck.lastCell != Origin) //if the cheking cell has a unit that is not mine? -> skip
                        continue;
            }

            if ((filters & NavFilter.IMPASSABLE_CELLS) == NavFilter.IMPASSABLE_CELLS) // filtering out immpassable cells
                if (Origin.GetUnit().GetImpassableCells().Contains(currentCheck.lastCell.GetCellType())) // if this unit cant pass off this cheking cell -> skip
                    continue;

            Closed.Add(currentCheck.lastCell);

            if ((filters & NavFilter.NOT_EMPTY_UNIT) == NavFilter.NOT_EMPTY_UNIT) //Restrict is empty
                if(!currentCheck.lastCell.GetUnit())
                    Closed.Remove(currentCheck.lastCell);

            if ((filters & NavFilter.NOT_ORIGIN) == NavFilter.NOT_ORIGIN && currentCheck.lastCell == Origin) // Restrict Origin            
                Closed.Remove(currentCheck.lastCell);

            foreach (WorldMap_Cell c in currentCheck.lastCell.GetNeighbors())
            {
                if(c)
                {
                    WorldMap_Cell c_2 = c;
                    Pathfinder_CellCheck newCellCheck = new Pathfinder_CellCheck(currentCheck);

                    if ((filters & NavFilter.MOVEMENT) == NavFilter.MOVEMENT) // filtering if it's for movement
                        newCellCheck.AddCell(c_2, c_2.GetMovementCost());
                    else if ((filters & NavFilter.ATTACK_VISION) == NavFilter.ATTACK_VISION)  // filtering if it's for AttackOrder
                        newCellCheck.AddCell(c_2, 1);

                        // setting up the heutistics
                    if (c_2.GetHeuristic() == -1)
                    {
                        if (c_2.IsNeighborOf(Origin)) // if is a neighbor of origin cell?
                        {
                            c_2.SetHeuristic(1);
                        }
                        else
                        {
                            foreach (WorldMap_Cell n_C2 in c_2.GetNeighbors())
                            {
                                if (n_C2)
                                {
                                    if (n_C2.GetHeuristic() != -1)
                                    {
                                        c_2.SetHeuristic( n_C2.GetHeuristic() + 1);
                                    }
                                }
                            }
                        }
                    }
                    Open.Add(newCellCheck);
                }
            }
        }

        return Closed;
    } //Expandible from Origin
    public List<WorldMap_Cell> GetCellsRange(WorldMap_Cell Origin, int MinRange, int MaxRange , NavFilter filters, int AddRemoveFog)
    {
        if(MinRange > MaxRange)
        {
            Debug.LogError("Min Range is farer than Max Range");
            return null;
        }
        else
        {
            if (MinRange == 1)
                return GetCellsRange(Origin, MaxRange, filters, AddRemoveFog);
            else
            {
                List<WorldMap_Cell> min = GetCellsRange(Origin, MinRange - 1, filters, AddRemoveFog);
                List<WorldMap_Cell> max = GetCellsRange(Origin, MaxRange, filters, AddRemoveFog);

                List<WorldMap_Cell> r = max.Except(min).ToList();

                return r;
            }
        }
    } //range like a donout
    public List<WorldMap_Cell> ExpandRange(List<WorldMap_Cell> CurrentRange, int BonusRange)
    {
        List<WorldMap_Cell> result = new List<WorldMap_Cell>();

        for (int i = BonusRange; i > 0; i--)
        {
            List<WorldMap_Cell> Aux = new List<WorldMap_Cell>();
            foreach (WorldMap_Cell CheckingCell in CurrentRange)
            {                
                foreach (WorldMap_Cell n in CheckingCell.GetNeighbors())
                {
                    if (n)
                    {
                        if (!CurrentRange.Contains(n) && !Aux.Contains(n))
                            Aux.Add(n);
                    }
                }
            }
            CurrentRange.AddRange(Aux);
        }
        result.AddRange(CurrentRange);
        return result;
    }  //if we have already a selection of cells we can expand that selection 
    public WorldMap_Cell DirectionalSearch(WorldMap_Cell Origin, List<WorldMap_Cell> Coincidences, Directions4 direction)
    {
        List<WorldMap_Cell> CheckingCells = new List<WorldMap_Cell>();
        List<WorldMap_Cell> CheckingCellsAux = new List<WorldMap_Cell>();
        Vector2 DirectionVector = new Vector2();

        switch(direction)
        {
            case Directions4.NORTH:
                DirectionVector = new Vector2(0, 1);
                break;
            case Directions4.SOUNTH:
                DirectionVector = new Vector2(0, -1);
                break;
            case Directions4.EAST:
                DirectionVector = new Vector2(1, 0);
                break;
            case Directions4.WEAST:
                DirectionVector = new Vector2(-1, 0);
                break;
        }

        if (GameManager.Instance.Map.getWorldCell(Origin.GetPosition() + DirectionVector))
            CheckingCells.Add(GameManager.Instance.Map.getWorldCell(Origin.GetPosition() + DirectionVector));
        else
            return null;

        for(int i = 0; i < CheckingCells.Count; i++ )
        {
            if (Coincidences.Contains(CheckingCells[i]))
                return CheckingCells[i];
            else
            {
                foreach(WorldMap_Cell NC in CheckingCells[i].GetNeighbors())
                {
                    if (NC)
                    {                        
                        if (Coincidences.Contains(NC) && !NC.Equals(Origin))
                            return NC;
                        else if(!NC.Equals(Origin) && !CheckingCells.Contains(NC) && !CheckingCellsAux.Contains(NC))
                        {
                            if (GameManager.Instance.Map.getWorldCell(NC.GetPosition() + DirectionVector))
                            {
                                CheckingCells.Add(GameManager.Instance.Map.getWorldCell(NC.GetPosition() + DirectionVector));
                                if (Coincidences.Contains(CheckingCells[CheckingCells.Count - 1]))
                                    return CheckingCells[CheckingCells.Count - 1];
                            }

                            CheckingCellsAux.Add(NC);
                        }
                        
                    }
                }
            }
        }

        return null;
    }

    // NavMesh
    public void LoadNavMesh(List<WorldMap_Cell> cells)
    {
        foreach(WorldMap_Cell cell in cells)
        {
            NavCell newnavcell = NavCellPool.Spawn().GetComponent<NavCell>();
            newnavcell.SetCell(cell);
        }
    }
    public void ResetNavMesh()
    {
        NavCellPool.ResetPool();
    }
    public bool IfThisCellIsInNav(WorldMap_Cell cell)
    {
        foreach(GameObject navActive in NavCellPool.GetPoolActive())
        {
            NavCell navCellActive = navActive.GetComponent<NavCell>();
            if (navCellActive.GetCell() == cell)
                return true;

        }
        return false;
    }
    
    // PathFinding
    public List<WorldMap_Cell> CalculatePath(WorldMap_Cell Origin, WorldMap_Cell Destination, int Resources)
    {
        return Tracer.CalculatePath(Origin, Destination, Resources);
    }
    public void LoadPathMesh(List<WorldMap_Cell> Path)
    {
        ResetPathMesh();
        foreach (WorldMap_Cell cell in Path)
        {
            TracerPathCell tracepathcell = TracerPathPool.Spawn().GetComponent<TracerPathCell>();
            tracepathcell.SetCell(cell);
        }
    }
    public void ResetPathMesh()
    {
        TracerPathPool.ResetPool();
    }

    // FireRange
    public void LoadFireRange(List<WorldMap_Cell> cells)
    {
        foreach (WorldMap_Cell cell in cells)
        {
            FireCell firecell = FireRangePool.Spawn().GetComponent<FireCell>();
            firecell.SetCell(cell);
        }
    }
    public void ResetFireCells()
    {
        FireRangePool.ResetPool();
    }
    public bool IfThisCellIsInFire(WorldMap_Cell cell)
    {
        foreach (GameObject fireActive in FireRangePool.GetPoolActive())
        {
            FireCell fireCellActive = fireActive.GetComponent<FireCell>();
            if (fireCellActive.GetCell() == cell)
                return true;

        }
        return false;
    }
}

public class Pathfinder_CellCheck
{
    public List<WorldMap_Cell> listCell = new List<WorldMap_Cell>();
    public int cost = 0;
    public WorldMap_Cell lastCell;

    public Pathfinder_CellCheck() { }

    public Pathfinder_CellCheck(Pathfinder_CellCheck cp)
    {
        listCell    = cp.listCell;
        cost        = cp.cost;
        lastCell    = cp.lastCell;
    }

    public Pathfinder_CellCheck(WorldMap_Cell c, int Cost)
    {
        cost += Cost;
        listCell.Add(c);
        lastCell = c;
    }

    public void AddCell(WorldMap_Cell c, int Cost)
    {
        cost += Cost;
        listCell.Add(c);
        lastCell = c;
    }
}// class which works for selection of range

public class Pathfinder_Tracer
{
    private List<WorldMap_Cell> Path = new List<WorldMap_Cell>();
    private List<WorldMap_Cell> Open = new List<WorldMap_Cell>();
    private List<WorldMap_Cell> Close = new List<WorldMap_Cell>();
    private WorldMap_Cell CheckingCell = null;
    private bool TargetFounded = false;

    private WorldMap_Cell _origin = null;
    private WorldMap_Cell _destination = null;

    public Pathfinder_Tracer() { }

    public List<WorldMap_Cell> CalculatePath(WorldMap_Cell Origin, WorldMap_Cell Destination, int resources)
    {
        _origin = Origin;
        if (_origin == Destination) //if is the same cell Origin and Destination  (filters?)
        {
            ResetPath();
            _destination = Destination;
            Path.Add(_destination);
        }
        else
        {
            if (_destination != Destination)
            {
                if (GameManager.Instance.Pathfinder.IfThisCellIsInNav(Destination)) // (filter?)
                {
                    if (Destination.IsNeighborOf(_destination)) // if the new destination is a neighbor of the old one (filter?)
                    {
                        if (Path.Contains(Destination)) // if the current path already contains this new destination
                        {
                            if (Path[1] == Destination) // if the new destination is the previus than the old one
                            {
                                // remove the old destination
                                _destination.SetParent(null);
                                _destination = Destination;
                                Path[0].SetCost(0);
                                Path.Remove(Path[0]);
                            }
                            else
                            {
                                // create new path
                                ResetPath();
                                _destination = Destination;
                                Path = SearchPath();
                            }
                        }
                        else
                        {
                            int new_G_Cost = _destination.GetCost() + Destination.GetMovementCost(); // movement!! (filter?)

                            if (resources >= new_G_Cost) // if the Resouces of the unit is enough for the new cost
                            {
                                //add new destination
                                _destination = Destination;
                                _destination.SetCost(new_G_Cost);
                                _destination.SetParent(Path[0]);
                                Path.Insert(0, _destination);
                            }
                            else
                            {
                                //create new path
                                ResetPath();
                                _destination = Destination;
                                Path = SearchPath();
                            }
                        }
                    }
                    else
                    {
                        // create new path
                        ResetPath();
                        _destination = Destination;
                        Path = SearchPath();
                    }
                }
            }
        }
        
        return Path;
    }

    public void ResetPath()
    {
        Path.ForEach(delegate (WorldMap_Cell cell)
        {
            cell.SetParent(null);
            cell.SetCost(0);
        });
        Path.Clear();
    }

    List<WorldMap_Cell> SearchPath()
    {
        //Initialization
        List<WorldMap_Cell> ResultPath = new List<WorldMap_Cell>();
        CheckingCell = _origin;
        TargetFounded = false;

        //Search
        while (!TargetFounded)
        {
            Pathfind();
        }

        //Remove Lists
        Open.Clear();
        Close.Clear();
        ResultPath = TracePath();

        return ResultPath;
    }

    void Pathfind()
    {
    // calcular valores de los vecinos
        foreach (WorldMap_Cell node in CheckingCell.GetNeighbors())
        {
            if (node && CheckingCell)
            {
                DetermineValues(CheckingCell, node);
                if (TargetFounded)
                    return;
            }
        }

        // si no se encontro el target en estos vecinos encontrar otro target
        if (!TargetFounded)
        {            
            Close.Add(CheckingCell);
            Open.Remove(CheckingCell);
            CheckingCell = GetSmallest_F_Value();
        }
    }

    void DetermineValues(WorldMap_Cell current, WorldMap_Cell test)
    {
        if (!test)
            return;

        if (test == _destination)
        {
            int newCost_G = current.GetCost() + test.GetMovementCost();
            test.SetCost(newCost_G);
            test.SetParent(current);
            TargetFounded = true;
            return;
        }

        if (!GameManager.Instance.Pathfinder.IfThisCellIsInNav(test))
            return;

        if (!Close.Contains(test))
        {
            if (Open.Contains(test))
            {
                int newCost_G = current.GetCost() + test.GetMovementCost();
                if (newCost_G < test.GetCost())
                {
                    test.SetParent(current);
                    test.SetCost(newCost_G);
                }
            }
            else
            {
                test.SetParent(current);
                test.SetCost(current.GetCost() + test.GetMovementCost());
                Open.Add(test);
            }
        }
    }

    WorldMap_Cell GetSmallest_F_Value()
    {
        WorldMap_Cell result = null;
        foreach (WorldMap_Cell o in Open)
        {
            if (!result)
            {
                result = o;
            }
            else
            {
                if (result.GetFinalCost() > o.GetFinalCost())
                    result = o;
            }
        }

        return result;
    }

    List<WorldMap_Cell> TracePath()
    {
        // to correct
        List<WorldMap_Cell> result = new List<WorldMap_Cell>();
        WorldMap_Cell node = _destination;
        do
        {
            if (node)
            {
                result.Add(node);
                node = node.GetParent();
            }
            else
            {
                return result;
            }
        }
        while (node);
        return result;

    }

}// class where it works all about pathfinding

