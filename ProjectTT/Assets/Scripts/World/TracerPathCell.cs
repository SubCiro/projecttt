﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//CHECK PLEASE IS LIKE NAVCELL
public class TracerPathCell : MonoBehaviour {

    WorldMap_Cell cell;

    public WorldMap_Cell GetCell()
    {
        return cell;
    }

    public void SetCell(WorldMap_Cell Cell)
    {
        cell = Cell;
        transform.position = cell.transform.position;
        GetComponent<SpriteRenderer>().sortingLayerName = Manager.DrawGameLayers.Pathfinder.ToString(); //this is the unique diference
        GetComponent<SpriteRenderer>().sortingOrder = cell.GetComponent<SpriteRenderer>().sortingOrder;
    }
}
