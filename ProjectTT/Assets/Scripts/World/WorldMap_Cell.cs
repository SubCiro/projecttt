﻿using UnityEngine;
using System.Collections;

public class WorldMap_Cell : MonoBehaviour {
    WorldMap_Cell_Info Info; // Cell Gameplay Information
    WorldMap_Cell_Navigation Navigation; //Cell Navigation Info.    

    //INITIALIZERS
    public void Init(WorldMap_Cell_Info.TerrainType type, Vector2 pos, int OrderInlayer)
    {
        Info = new WorldMap_Cell_Info(type, pos, OrderInlayer);
        GetComponent<SpriteRenderer>().sortingOrder = OrderInlayer;
    }
    public void InitNavigation(Vector2 pos)
    {
        Navigation = new WorldMap_Cell_Navigation(pos);
    }    

    //UNIT
    public void SetUnit(Unit unit)
    {
        Info.CellUnit = unit;

        if (Info.CellUnit)
        {
            Info.CellUnit.SetOrderInlayer(Info.OrderInlayer);
            Info.CellUnit.SetCurrentPosition(this); //tentative for pathfinding services
        }
    }
    public Unit GetUnit()
    {
        return Info.CellUnit;
    }    

    //INFO
    public WorldMap_Cell_Info.TerrainType GetCellType()
    {
        return Info.Type;
    }
    public Vector2 GetPosition()
    {
        return Info.PosMap;
    }
    public int GetOrderedinLayer()
    {
        return Info.OrderInlayer;
    }

    //NEIGHBORS
    public bool IsNeighborOf(WorldMap_Cell cell)
    {
        return Navigation.IsNeighborOf(cell);
    }
    public WorldMap_Cell[] GetNeighbors()
    {
        return Navigation.GetNeighbors();
    }

    //HEURISTICS
    public int GetHeuristic()
    {
        return Navigation.GetHeuristic();
    }
    public void SetHeuristic(int value)
    {
        Navigation.SetHeuristic(value);
    }

    //COSTS - PathFinding
    public int GetCost()
    { return Navigation.GetCost(); }
    public void SetCost(int Value)
    { Navigation.SetCost(Value); }
    public int GetFinalCost()
    {
        return Navigation.GetFinalCost();
    }
    public int GetMovementCost() //used on selection range... we need to change it
    {
        return Info.RestMove;
    }
    public WorldMap_Cell GetParent()
    {
        return Navigation.GetParent();
    }
    public void SetParent(WorldMap_Cell Parent)
    {
        Navigation.SetParent(Parent);
    }

    //Fog of War
    bool FogOfWar_Cell; //boolean whose represent if this cell is highlighted or not
    public void SetFogCell(bool CellStatus)
    {
        FogOfWar_Cell = CellStatus;
        if (FogOfWar_Cell)
        {
            GetComponent<SpriteRenderer>().color = Color.white;
            if (GetUnit())
                GetUnit().SetVisible(true);

        }
        else
        {
            GetComponent<SpriteRenderer>().color = Color.gray;
            if (GetUnit())
                GetUnit().SetVisible(false);
        }

    }
    public bool IsHighLighted()
    {
        return FogOfWar_Cell;
    } //is highlighted or not
    bool Transited;
    int VisionRange;
    public void SetTransited(bool transStatus, int Range)
    {
        Transited = transStatus;
        if (Transited)
        {
            if (VisionRange < Range)
            {
                VisionRange = Range;
            }
        }
        else
        {
            VisionRange = 0;
        }
    }
    public bool WasTransited()
    {
        return Transited;
    }
    public int GetRangeTransitedVision()
    {
        return VisionRange;
    }
}

[System.Serializable]
public class WorldMap_Cell_Info
{
    // Campos
    public enum TerrainType // Terrains Types Definition value is equals to RestMove_Catalog index
    {
        Grass = 0,
    };

    public static int[] RestMove_Catalog = 	// Movement resistance 
	{
        1,	// Grass
    };

    public TerrainType Type;    // Cell type
    public int RestMove;        // Cell movement resistance 
    public Vector2 PosMap;      // Cell Position
    public Unit CellUnit;       // Unit whose is in this Cell
    public int OrderInlayer;

    public WorldMap_Cell_Info(TerrainType type, Vector2 pos, int orderInlayer) // Set default values with position
    {
        Type = type;
        RestMove = RestMove_Catalog[(int)type];
        PosMap = pos;
        OrderInlayer = orderInlayer;
        CellUnit = null;
    }
}

//Check please
[System.Serializable]
public class WorldMap_Cell_Navigation
{
    int heuristic   = -1;
    int cost        = 0;
    WorldMap_Cell[] neighbors; // this array keeps the neightbor of north, south, east and weast
    WorldMap_Cell parent = null;

    public WorldMap_Cell_Navigation(Vector2 position)
    {
        neighbors = new WorldMap_Cell[4];
        Vector2[] neighbors_index = { new Vector2 (1, 0), new Vector2(0, 1), new Vector2(-1, 0), new Vector2(0, -1) };

        int i = 0;
        foreach (Vector2 neighbor_index in neighbors_index)
        {
            float Dif_Width = neighbor_index.x + position.y;
            float Dif_Height = neighbor_index.y + position.x;

            if (Dif_Width >= 0 && Dif_Width <= WorldMap.Instance.GetWorldSize().x - 1)
            {
                if (Dif_Height >= 0 && Dif_Height <= WorldMap.Instance.GetWorldSize().y - 1)
                {
                    neighbors[i] = WorldMap.Instance.WorldCells[(int)Dif_Width][(int)Dif_Height];
                }
            }
            i++;
        }
    } // set neightbors when the maps is just created

    public WorldMap_Cell[] GetNeighbors()
    {
        return neighbors;
    }

    public bool IsNeighborOf(WorldMap_Cell cell)
    {
        foreach (WorldMap_Cell Neighbor in neighbors)
        {
            if (Neighbor)
            {
                if (Neighbor == cell)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void SetHeuristic(int value)
    {
        heuristic = value;
    }

    public int GetHeuristic()
    {
        return heuristic;
    }

    public int GetCost()
    { return  cost; }

    public int GetFinalCost()
    {
        return heuristic + cost;
    }

    public void SetCost(int Value)
    { cost = Value; }

    public void SetParent(WorldMap_Cell Parent)
    {
        parent = Parent;
    }

    public WorldMap_Cell GetParent()
    {
        return parent;
    }
}
