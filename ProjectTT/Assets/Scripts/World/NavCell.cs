﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavCell : MonoBehaviour {

    WorldMap_Cell cell;

    public WorldMap_Cell GetCell()
    {
        return cell;
    }

    public void SetCell(WorldMap_Cell Cell)
    {
        cell = Cell;
        transform.position = cell.transform.position;
        GetComponent<SpriteRenderer>().sortingLayerName = Manager.DrawGameLayers.Nav.ToString();
        GetComponent<SpriteRenderer>().sortingOrder = cell.GetComponent<SpriteRenderer>().sortingOrder; // TODO: not use getcomponent
    }
}
